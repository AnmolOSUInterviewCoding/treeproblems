package com.company;

import java.util.StringTokenizer;

/**
 * Created by Anmol on 2/1/2016.
 */
public class PreOrderSerializationLeetCode {

    public static void main(String[] args) {
//        String s = "9,3,4,#,#,1,#,#,2,#,6,#,#";
        String s = "#";
        String[] split = s.split(",");
        int count = 0;
        for(int i = 0; i< split.length;i++) {
            if(split[i].equals("#")){
                count--;
                if(count == -1 && i!=split.length-1) {
                    System.out.print("false");
                    break;
                }
            } else {
                count++;
            }
        }
        if(count == -1){
            System.out.print("true");
        } else {
            System.out.print("false");
        }
    }
}
