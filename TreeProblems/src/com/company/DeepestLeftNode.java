package com.company;

/**
 * Created by Anmol on 2/20/2016.
 */
public class DeepestLeftNode {
    static TreeProblems node = null;

    public static void main(String[] args) {
        getDeepestLeftNode(TreeProblems.getRoot(), false, 0, 0);
        System.out.print(node.val);
    }

    private static void getDeepestLeftNode(TreeProblems root, boolean isLeft, int level, int maxLevel) {
        if (root == null) return;

        if (isLeft && root.left == null && root.right == null && level > maxLevel) {
            maxLevel = level;
            node = root;
        }
        getDeepestLeftNode(root.left, true, level + 1, maxLevel);
        getDeepestLeftNode(root.right, false, level + 1, maxLevel);
    }
}
