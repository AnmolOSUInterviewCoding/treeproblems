package com.company;

/**
 * Created by Anmol on 2/20/2016.
 */
public class SumTree {

    public static void main(String[] args) {
        TreeProblems binaryTree = TreeProblems.getBinaryTree();
        toSumTree(binaryTree);
    }
    static int toSumTree(TreeProblems node) {

        // Base case
        if (node == null) {
            return 0;
        }

        // Store the old value
        int old_val = node.val;

        // Recursively call for left and right subtrees and store the sum as
        // new value of this node
        node.val = toSumTree(node.left) + toSumTree(node.right);

        // Return the sum of values of nodes in left and right subtrees and
        // old_value of this node
        return node.val + old_val;
    }
}