package com.company;

/**
 * Created by Anmol on 1/25/2016.
 */
public class RemoveNodesNotInPathSum {

    public static void main(String[] args) {
        TreeProblems root = new TreeProblems(1);
        root.left = new TreeProblems(2);
        root.right =  new TreeProblems(3);
        root.left.left =  new TreeProblems(4);
        root.left.right =  new TreeProblems(5);
        root.right.left =  new TreeProblems(6);
        root.right.right =  new TreeProblems(7);
        root.left.left.left =  new TreeProblems(8);
        root.left.left.right =  new TreeProblems(9);
        root.left.right.left =  new TreeProblems(12);
        root.right.right.left =  new TreeProblems(10);
        root.right.right.left.right =  new TreeProblems(11);
        root.left.left.right.left =  new TreeProblems(13);
        root.left.left.right.right =  new TreeProblems(14);
        root.left.left.right.right.left =  new TreeProblems(15);

        int val = 45;
        removeNodesNotInSumPath(root, 0, val);
    }

    private static TreeProblems removeNodesNotInSumPath(TreeProblems root, int sumTillHere, int val) {
        if(root == null) return null;
        sumTillHere +=root.val;
        if(sumTillHere >= val) return root;
        root.left = removeNodesNotInSumPath(root.left, sumTillHere, val);

        root.right =  removeNodesNotInSumPath(root.right, sumTillHere, val);
        if(root.left==null && root.right == null && sumTillHere<val) {
            //sumTillHere -=root.val;
            return null;
        }
        return root;
    }
}
