package com.company;

/**
 * Created by Anmol on 1/23/2016.
 */
public class SortedLinkListToBST {
    static LinkedListProblems temp;
    public static void main(String[] args) {
        LinkedListProblems head = new LinkedListProblems(1);
        head.next = new LinkedListProblems(2);
        head.next.next = new LinkedListProblems(3);
        head.next.next.next = new LinkedListProblems(4);
        head.next.next.next.next = new LinkedListProblems(5);
        TreeProblems treeProblems = sortedListToBST(head);
        System.out.println("Sorted");

    }

    private static TreeProblems sortedListToBST(LinkedListProblems head) {

        if(head == null) return null;
        temp = head;
        int len = findLength(head);
        return sortedListToBST(0, len-1);
    }

    private static int findLength(LinkedListProblems head) {
        LinkedListProblems temp = head;
        int len = 0;
        while(temp!=null) {
            len++;
            temp = temp.next;
        }
        return len;
    }

    private static TreeProblems sortedListToBST(int start, int end) {
        if(start>end) return null;

        int mid = (start+end)/2;
        TreeProblems left = sortedListToBST(start, mid-1);
        TreeProblems root = new TreeProblems(temp.val);
        temp = temp.next;
        TreeProblems right = sortedListToBST(mid+1,end);

        root.left = left;
        root.right = right;
        return root;

    }

    public static class LinkedListProblems {
        int val;
        LinkedListProblems next;

        LinkedListProblems(int val) {
            this.val = val;
        }

    }
}
