package com.company;

/**
 * Created by Anmol on 1/23/2016.
 */
public class ConstructBSTFromInOrder {

    public static void main(String []args) {
        int[] arr = new int[]{1, 5, 10, 40, 30, 15, 28, 20};

        TreeProblems root = getBST(arr, 0, arr.length-1);
    }

    private static TreeProblems getBST(int[] arr, int start, int end) {
        if(start>end) return null;

        int mid = (start+ end)/2;
        TreeProblems root = new TreeProblems(arr[mid]);
        root.left = getBST(arr, start, mid-1);
        root.right = getBST(arr, mid+1, end);

        return root;
    }
}
