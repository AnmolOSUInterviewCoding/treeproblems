package com.company;

/**
 * Created by Anmol on 1/23/2016.
 */
public class ConvertTreeChildrenSumProperty {
    public static void main(String[] args) {
        TreeProblems root = new TreeProblems(50);
        root.left = new TreeProblems(7);
        root.right = new TreeProblems(2);
        root.left.left = new TreeProblems(3);
        root.left.right = new TreeProblems(5);
        root.right.left = new TreeProblems(1);
        root.right.right = new TreeProblems(30);
        childrenSumConversion(root);
    }

    private static void childrenSumConversion(TreeProblems root) {
        if(root ==null|| (root.left==null && root.right== null)) return;
        childrenSumConversion(root.left);
        childrenSumConversion(root.right);

        int leftSum = root.left!=null ? root.left.val: 0;
        int rightSum = root.right!=null ? root.right.val: 0;

        if(root.val < (leftSum+rightSum)) {
            root.val += (leftSum+rightSum) - root.val;
        } else if(root.val > (leftSum+rightSum)) {
            if(root.left!=null) {
                root.left.val += root.val-(leftSum+rightSum);
                childrenSumConversion(root.left);
            } else if(root.right!=null){
                root.right.val += root.val-(leftSum+rightSum);
                childrenSumConversion(root.right);
            } else {
                root.val += root.val-(leftSum+rightSum);
            }
        }
    }
}
