package com.company;

/**
 * Created by Anmol on 1/20/2016.
 */
public class TreeProblems {
    int val;
    TreeProblems left;
    TreeProblems right;
    TreeProblems nextRight;
    int liss;
    int horizontalDistance;

    public TreeProblems(int val) {
        this.val = val;
        liss = 0;
    }
    public static TreeProblems getBinaryTree() {
        TreeProblems root = new TreeProblems(1);
        root.left = new TreeProblems(2);
        root.right =  new TreeProblems(3);
        root.left.left =  new TreeProblems(4);
        root.left.right =  new TreeProblems(5);
        root.right.left =  new TreeProblems(6);
        root.right.right =  new TreeProblems(7);
        return root;
    }
    public static TreeProblems getRoot() {
        TreeProblems root = new TreeProblems(4);
        root.left = new TreeProblems(2);
        root.left.left = new TreeProblems(1);
        root.left.left.left = new TreeProblems(7);
        root.left.right = new TreeProblems(3);
        root.right = new TreeProblems(5);
        root.right.right = new TreeProblems(6);
        root.right.right.right = new TreeProblems(8);
        root.right.right.right.right = new TreeProblems(10);
        root.right.right.right.right.left = new TreeProblems(9);
        return root;
    }
}
