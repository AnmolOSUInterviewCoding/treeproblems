package com.company;

/**
 * Created by Anmol on 2/20/2016.
 */
public class ExctractLeavesInDLL {
    static DoubleLinkList head= null,next, prev;
    private static class DoubleLinkList {
        int val;
        DoubleLinkList next;
        DoubleLinkList prev;

        private DoubleLinkList(int val) {
            this.val = val;
        }
    }

    public static void main(String[] args) {

        leafNodesToDoubleLL(TreeProblems.getRoot());

    }

    private static void leafNodesToDoubleLL(TreeProblems root) {
        if(root == null) return;

        leafNodesToDoubleLL(root.left);
        leafNodesToDoubleLL(root.right);
        if(root.left == null && root.right==null) {
            if(head == null) {
                head = new DoubleLinkList(root.val);
                next = head;
            } else {
                next.next = new DoubleLinkList(root.val);
                prev = next.next;
                prev.prev = next;
                next = prev;
            }
        }
    }
}
