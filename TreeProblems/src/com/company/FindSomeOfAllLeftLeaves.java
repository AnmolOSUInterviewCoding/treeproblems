package com.company;

/**
 * Created by Anmol on 2/1/2016.
 */
public class FindSomeOfAllLeftLeaves {
    private static int sum;
    public static void main(String[] args) {
//        findSomeOfAllLeftLEaves(TreeProblems.getBinaryTree());
        TreeProblems root = new TreeProblems(20);
        root.left = new TreeProblems(9);
        root.right = new TreeProblems(49);
        root.left.right = new TreeProblems(12);
        root.left.left = new TreeProblems(5);
        root.right.left = new TreeProblems(23);
        root.right.right = new TreeProblems(52);
        root.left.right.right = new TreeProblems(12);
        root.right.right.left = new TreeProblems(50);
        findSomeOfAllLeftLEaves(root);
        System.out.print(sum);
    }

    private static void findSomeOfAllLeftLEaves(TreeProblems root) {
        if(root == null) return;

        if(isLeftLeaf(root.left)) {
            sum += root.left.val;
        } else {
            findSomeOfAllLeftLEaves(root.left);
        }
        findSomeOfAllLeftLEaves(root.right);

    }

    private static boolean isLeftLeaf(TreeProblems left) {
        if(left == null) return false;
        return (left.left==null) && (left.right ==null);
    }

}
