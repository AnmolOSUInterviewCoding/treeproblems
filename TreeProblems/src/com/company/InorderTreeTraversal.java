package com.company;

import java.util.Stack;

/**
 * Created by Anmol on 1/20/2016.
 */
public class InorderTreeTraversal {

    public static void main(String[] args) {

        inorderTraversal(TreeProblems.getRoot());
        System.out.println("PreOrder is: ");
        preOrderTraversal(TreeProblems.getRoot());
        System.out.println("PostOrder is: ");
        //postOrderTraversal(TreeProblems.getRoot());
        postOrder(TreeProblems.getRoot());
    }

    private static void postOrder(TreeProblems root) {
        Stack<TreeProblems> stack = new Stack<>();
        TreeProblems curr = root;
        while (true) {
            if (curr != null) {
                if (curr.right != null) {
                    stack.push(curr.right);
                }
                stack.push(curr);
                curr = curr.left;
            } else {
                TreeProblems temp = stack.pop();
                if (!stack.empty() && temp.right == stack.peek()) {
                    TreeProblems node = stack.pop();
                    stack.push(temp);
                    curr = node;
                } else if (stack.empty()) {
                    System.out.print(temp.val+" ");
                    break;
                } else {
                    System.out.print(temp.val+" ");
                }
            }
        }
    }

    private static void postOrderTraversal(TreeProblems root) {
        if (root == null) return;
        postOrderTraversal(root.left);
        postOrderTraversal(root.right);
        System.out.print(root.val + " ");

    }

    public static void preOrderTraversal(TreeProblems root) {
        if (root == null) return;
        System.out.print(root.val + " ");
        preOrderTraversal(root.left);
        preOrderTraversal(root.right);
    }

    private static void inorderTraversal(TreeProblems root) {
        Stack<TreeProblems> stack = new Stack<TreeProblems>();
        stack.push(root);
        while (!stack.empty()) {
            if (root.left != null) {
                root = root.left;
                stack.push(root);
            } else {
                TreeProblems temp = stack.pop();
                System.out.println(temp.val);
                if (temp.right != null) {
                    root = temp.right;
                    stack.push(root);
                }
            }
        }

    }

}
