package com.company;

/**
 * Created by Anmol on 2/1/2016.
 */
public class ChangeRootToLeftSumTree {

    public static void main(String[] args) {
        TreeProblems binaryTree = TreeProblems.getBinaryTree();
        changeRootToLeftSum(binaryTree);
    }

    private static int changeRootToLeftSum(TreeProblems root) {
        if(root == null) return 0;
        if(root.left == null && root.right == null) {
            return root.val;
        }
        int leftSum = changeRootToLeftSum(root.left);
        int rightSum = changeRootToLeftSum(root.right);
        root.val += leftSum;
        return  root.val + rightSum;

    }
}
