package com.company;

/**
 * Created by Anmol on 1/22/2016.
 */
public class IsValidBST {

    public static void main(String[] args) {
        isValidBST(TreeProblems.getRoot());

    }

    private static boolean isValidBST(TreeProblems root) {
        if(root == null) return true;

        int max = getMax(root.left);
        int min = getMin(root.right);

        return root.val>= max &&root.val<=min && isValidBST(root.left)&&isValidBST(root.right);

    }

    public static int getMax(TreeProblems root) {
        if(root == null) return Integer.MIN_VALUE;

        int max = Integer.MIN_VALUE;
        max = Math.max(root.val, max);

        return Math.max(max, Math.max(getMax(root.left), getMax(root.right)));
    }

    public static int getMin(TreeProblems root) {
        if(root == null) return Integer.MAX_VALUE;

        int max = Integer.MAX_VALUE;
        max = Math.min(root.val, max);

        return Math.min(max, Math.min(getMin(root.left), getMin(root.right)));
    }
}
