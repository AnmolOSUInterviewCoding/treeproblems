package com.company;

/**
 * Created by Anmol on 1/23/2016.
 */
public class HeightOfTheTree {

    public static void main(String[] args) {
        System.out.print("Height of tree is: " +heightOfTree(TreeProblems.getRoot()));
    }

    private static int heightOfTree(TreeProblems root) {
        if(root==null) {
            return 0;
        }
        int lHeight = heightOfTree(root.left);
        int rHeight = heightOfTree(root.right);
        return (Math.max(lHeight, rHeight)+1);
    }

}
