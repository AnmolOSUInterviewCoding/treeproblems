package com.company;

/**
 * Created by Anmol on 1/23/2016.
 */
public class PreOrderToBinaryTree {
    static int index;
    public static void main(String[] args) {
        int[] arr = new int[]{4,2,1,3,5,6,8};
        char[] pre = new char[]{'N','N','L','L','N','L','L'};

        TreeProblems root = createTree(arr, pre, arr.length);
    }

    private static TreeProblems createTree(int[] arr, char[] pre, int length) {
        if(index == length) return null;

        TreeProblems root = new TreeProblems(arr[index]);
        if(pre[index] == 'N') {
            index++;
            root.left = createTree(arr, pre, length);
            index++;
            root.right = createTree(arr, pre, length);
        }
        return root;


    }
}
