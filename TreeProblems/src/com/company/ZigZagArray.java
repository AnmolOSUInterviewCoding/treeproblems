package com.company;

import java.util.Arrays;

/**
 * Created by Anmol on 2/28/2016.
 */
public class ZigZagArray {
    public static void main(String[] args) {
//        int arr[] = new int[]{5,2,8,7,-2,25,25};
        int arr[] = new int[]{5,2,56,4};

        Arrays.sort(arr);
        int[] result = new int[arr.length];
        int start = 0;
        int end = arr.length-1;
        int i =0;
        while (start<end) {
            result[i++] = arr[end];
            result[i++] = arr[start];
            end--;
            start++;

        }
        if(arr.length %2!=0) {
            result[i] = arr[start];
        }
        for(int num : result) {
            System.out.print(num +" ");
        }
    }
}
