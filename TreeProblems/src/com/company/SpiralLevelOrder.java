package com.company;

import java.util.Stack;

/**
 * Created by Anmol on 2/20/2016.
 */
public class SpiralLevelOrder {

    public static void main(String[] args) {
        levelOrderSpiral(TreeProblems.getBinaryTree());
    }

    private static void levelOrderSpiral(TreeProblems binaryTree) {
        Stack<TreeProblems> st1 = new Stack<>();
        Stack<TreeProblems> st2 = new Stack<>();

        st1.push(binaryTree);

        while (!st1.empty() || !st2.empty()) {
            while (!st1.empty()) {
                TreeProblems temp = st1.pop();
                System.out.print(temp.val +" ");
                if(temp.right!=null) {
                    st2.push(temp.right);
                }
                if(temp.left!=null) {
                    st2.push(temp.left);
                }
            }
            System.out.println();
            while (!st2.empty()) {
                TreeProblems temp = st2.pop();
                System.out.print(temp.val +" ");

                if(temp.left!=null) {
                    st1.push(temp.left);
                }
                if(temp.right!=null) {
                    st1.push(temp.right);
                }
            }
            System.out.println();
        }
    }
}
