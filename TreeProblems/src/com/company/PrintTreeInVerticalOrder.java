package com.company;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Anmol on 1/26/2016.
 */
public class PrintTreeInVerticalOrder {

    public static void main(String[] args) {
        HashMap<Integer, ArrayList<Integer>> nodesMap = new HashMap<>();
        nodesAtSameVerticalLevel(TreeProblems.getBinaryTree(),0, nodesMap);

    }

    private static void nodesAtSameVerticalLevel(TreeProblems root, int level, HashMap<Integer, ArrayList<Integer>> nodesMap) {
        if(root == null) return;

        if(nodesMap.containsKey(level)) {
            ArrayList<Integer> list = nodesMap.get(level);
            list.add(root.val);
            nodesMap.put(level, list);
        } else {
            ArrayList<Integer> list = new ArrayList<>();
            list.add(root.val);
            nodesMap.put(level, list);
        }
        nodesAtSameVerticalLevel(root.left, level -1, nodesMap);
        nodesAtSameVerticalLevel(root.right, level +1, nodesMap);
    }
}
