package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by Anmol on 1/25/2016.
 */
public class DigitArrayInterpretation2Alpha {

    public static void main(String [] args) {
        //int digits[] = {1, 2, 2, 1, 3, 2, 1};
        //int digits[] = {1, 2, 2, 1};
        //int digits[] = {1, 1};
//        int digits[] = {9, 1, 8};
        int digits[] = {1, 2,2,1};
        solution (digits);
    }

    public static void solution(int[] digits)     {
        ArrayList<ArrayList<String>> allSequences = new ArrayList<>();

        for(int i = 0; i < digits.length; i++)  {
            if(digits[i] == 0 || digits[i] > 26 || digits[i] < 0)  {
                return;
            }

            String digitStr = String.valueOf (digits[i]);

            /*1st Base Condition*/
            if(allSequences.size () == 0)  {
                ArrayList<String> tempSequences = new ArrayList<> ();
                tempSequences.add (digitStr);
                allSequences.add (tempSequences);
            }
            /*2nd Base Condition*/
            else if(allSequences.size () == 1)   {
                ArrayList<String> tempSequences = new ArrayList<> ();
                tempSequences.add (allSequences.get (0).get (0) + "," + digitStr);
                int digit = digits[i-1] > 2 ? digits[i] : digits[i-1] * 10 + digits[i];
                tempSequences.add(String.valueOf(digit));
                allSequences.add (tempSequences);
            }else   {
                ArrayList<String> tempSequences = new ArrayList<> ();
                ArrayList<String> previousSequences = allSequences.get (i-1);
                tempSequences.addAll (previousSequences.stream ().map (aSequence -> aSequence + "," +
                        digitStr).collect (Collectors.toList()));
                int digit = digits[i-1] * 10 + digits[i];
                if(digit < 27)  {
                    previousSequences = allSequences.get (i-2);
                    tempSequences.addAll(previousSequences.stream ().map (aSequence -> aSequence + "," + String
                            .valueOf (digit)).collect(Collectors.toList()));
                }

                allSequences.add (tempSequences);
            }
        }

        ArrayList<String> finalSequences = allSequences.get (digits.length - 1);

        System.out.println("Number of Interpretations : " + finalSequences.size ());
        for(String aSequence : finalSequences)  {
            Arrays.asList(aSequence.split(",")).stream ().map(character -> String.valueOf ((char)(Integer
                    .parseInt (character) + 64))).forEach
                    (System.out::print);
            System.out.print("("+aSequence+")\n");
        }
    }
}