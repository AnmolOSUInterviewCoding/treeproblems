package com.company;

/**
 * Created by Anmol on 1/26/2016.
 */
public class ConstructTreeFromLevelOrderInOrder {

    public static void main(String[] args) {
        int[] levelOrder = new int[]{20, 8, 22, 4, 12, 10, 14};
        int[] inOrder = new int[]{4, 8, 10, 12, 14, 20, 22};

        TreeProblems root = createTree(null, levelOrder, inOrder, 0, inOrder.length-1);
    }

    private static TreeProblems createTree(TreeProblems root,int[] levelOrder, int[] inOrder, int start, int end) {
        if(start>end) return null;
        if(start == end) {
            return new TreeProblems(inOrder[start]);
        }

        boolean found = false;
        int index = 0;

        // it represents the index in inOrder array of element that appear
        // first in levelOrder array.
        for (int i = 0; i < levelOrder.length - 1; i++) {
            int data = levelOrder[i];
            for (int j = start; j < end; j++) {
                if (data == inOrder[j]) {
                    root = new TreeProblems(data);
                    index = j;
                    found = true;
                    break;
                }
            }
            if (found) {
                break;
            }
        }

        root.left = createTree(root,levelOrder, inOrder, start, index-1);
        root.right = createTree(root,levelOrder, inOrder, index+1, end);
        return root;
    }

}
