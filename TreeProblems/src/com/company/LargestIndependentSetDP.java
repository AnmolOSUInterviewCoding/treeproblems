package com.company;

/**
 * Created by Anmol on 1/23/2016.
 */
public class LargestIndependentSetDP {

    public static void main(String[] args) {
        TreeProblems root = TreeProblems.getRoot();
        int largestIndependentSet = getLargestIndependentSet(root);
        System.out.println(largestIndependentSet);
        inOrderTraversal(root);
    }

    private static void inOrderTraversal(TreeProblems root) {
        if(root == null) return;
        inOrderTraversal(root.left);
        System.out.print(" Node: " + root.val + " liss: " + root.liss);
        inOrderTraversal(root.right);
    }

    private static int getLargestIndependentSet(TreeProblems root) {
        if(root == null) return 0;
        if(root.liss!=0) {
            return root.liss;
        }
        if(root.left==null&& root.right == null) {
            root.liss = 1;
        }
        //get liss excluding the root.
        int lissExcluding = getLargestIndependentSet(root.left) + getLargestIndependentSet(root.right);

        int lissIncluding = 1;

        //add the liss of grandchildren of root.
        if(root.left!=null) {
            lissIncluding += getLargestIndependentSet(root.left.left) + getLargestIndependentSet(root.left.right);
        }

        if(root.right!=null) {
            lissIncluding += getLargestIndependentSet(root.right.left) + getLargestIndependentSet(root.right.right);
        }
        root.liss = Math.max(lissExcluding, lissIncluding);
        return root.liss;
    }

}
