package com.company;

/**
 * Created by Anmol on 1/23/2016.
 */
public class PrintAncestors {

    public static void main(String[] args) {
        printAncestors(TreeProblems.getRoot(), 8);
    }

    private static boolean printAncestors(TreeProblems root, int val) {
        if(root == null) return false;

        if(root.val == val) {

            return true;
        }
        if(printAncestors(root.left, val)|| printAncestors(root.right, val)) {
            System.out.print(root.val + " ");
            return true;
        }
        return false;
    }

}
