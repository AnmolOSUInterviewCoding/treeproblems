package com.company;

/**
 * Created by Anmol on 2/3/2016.
 */
public class KthLargestElementBST {

    static int ele;
    public static void main(String[] args) {
        getKthLargest(TreeProblems.getRoot(), 3);
    }

    private static void getKthLargest(TreeProblems root, int k) {
        if(root == null || ele >= k) return;

        getKthLargest(root.right, k);
        ele++;
        if(k == ele) {
            System.out.print(root.val);
            return;
        }
        getKthLargest(root.left, k);
    }

}
