package com.company;

import java.util.ArrayList;

/**
 * Created by Anmol on 1/26/2016.
 */
public class PrintNodesKDistanceFromLeaf {

    public static void main(String[] args) {
        printNodes(TreeProblems.getBinaryTree(), 1, new int[100], 0);
    }

    private static void printNodes(TreeProblems root, int k, int[] integers, int index) {
        if(root ==null) return;

        if(root.left == null&& root.right == null) {
            System.out.print(integers[index-k]);
            return;
        }
        integers[index]= (root.val);
        printNodes(root.left, k, integers, index+1);
        printNodes(root.right, k, integers, index+1);

    }

}
