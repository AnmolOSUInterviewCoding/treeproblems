package com.company;

/**
 * Created by Anmol on 2/3/2016.
 */
public class ReverseInOrderTraversal {

    public static void main(String[] args) {
        reverseInOrder(TreeProblems.getRoot());
    }

    private static void reverseInOrder(TreeProblems root) {
        if(root == null) {
            return;
        }
        reverseInOrder(root.right);
        System.out.print(root.val +" ");
        reverseInOrder(root.left);
    }
}
