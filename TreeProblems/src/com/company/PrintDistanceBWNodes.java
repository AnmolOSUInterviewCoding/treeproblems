package com.company;

/**
 * Created by Anmol on 1/25/2016.
 */
public class PrintDistanceBWNodes {
    static int d1=-1, d2=-1, dist;
    public static void main(String[] args) {
        TreeProblems node = printDistanceBWNodes(TreeProblems.getBinaryTree(),2,4 ,0 );
        if(d1!=-1 && d2!=-1) {
            System.out.print("Distance is: " + dist);
        } else if(d1!=-1) {
            findLevel(node, 4, 0);
            System.out.print("Distance is: " + dist);
        } else {
            dist = findLevel(node, 2, 0);
            System.out.print("Distance is: " + dist);
        }
    }

    private static int findLevel(TreeProblems node, int val, int level) {
        if (node == null)
            return -1;

        // If key is present at root, or in left subtree or right subtree,
        // return true;
        if (node.val == val) {
            dist = level;
            return level;
        }

        int l = findLevel(node.left, val, level+1);
        return (l != -1)? l : findLevel(node.right, val, level+1);
    }

    private static TreeProblems printDistanceBWNodes(TreeProblems root, int val1, int val2, int level) {
        if(root == null) return null;
        if(root.val == val1) {
            d1= level;
            return root;
        }
        if(root.val == val2) {
            d2 = level;
            return root;
        }

        TreeProblems leftLca = printDistanceBWNodes(root.left, val1, val2, level+1);
        TreeProblems rightLca = printDistanceBWNodes(root.right, val1, val2, level+1);

        if(leftLca!=null && rightLca!=null) {
            dist = d1 + d2 - 2*level;
            return root;
        }

        return leftLca!=null ? leftLca : rightLca;

    }

}
