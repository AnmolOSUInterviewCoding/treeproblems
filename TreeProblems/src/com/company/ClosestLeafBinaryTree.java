package com.company;

/**
 * Created by Anmol on 2/21/2016.
 */
public class ClosestLeafBinaryTree {

    public static final void main(String[] args) {
        TreeProblems root = new TreeProblems(1);
        root.left = new TreeProblems(2);
        root.right = new TreeProblems(3);
        root.right.left = new TreeProblems(4);
        root.right.left.left = new TreeProblems(6);
        root.right.left.left.left = new TreeProblems(7);
        root.right.left.left.right = new TreeProblems(8);
        root.right.right = new TreeProblems(5);
        root.right.right.right = new TreeProblems(9);
        root.right.right.right.left = new TreeProblems(10);

        int closest = findClosest(root, 3, new TreeProblems[100], 0);
    }

    private static int findClosest(TreeProblems root, int k, TreeProblems[] ancestors, int index) {
        if(root == null) {
            return Integer.MIN_VALUE;
        }

        if(root.val == k) {
            int res = getClosesDown(root);
            for (int i = index - 1; i >= 0; i--) {
                res = Math.min(res, index - i + getClosesDown(ancestors[i]));
            }
            return res;
        }

        ancestors[index++] = root;

        return Math.min(findClosest(root.left, k, ancestors, index), findClosest(root.right, k, ancestors, index));
    }

    private static int getClosesDown(TreeProblems root) {
        if(root == null) return Integer.MAX_VALUE;

        if (root.left == null && root.right== null) {
            return 0;
        }
        return 1+ Math.min(getClosesDown(root.left), getClosesDown(root.right));
    }
}
