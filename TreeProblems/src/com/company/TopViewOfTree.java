package com.company;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

/**
 * Created by Anmol on 1/26/2016.
 */
public class TopViewOfTree {

    public static void main(String[] args) {
        TreeProblems root = TreeProblems.getBinaryTree();
        root.right.left.right =  new TreeProblems(8);
        root.right.left.right.right =  new TreeProblems(9);
        root.right.left.right.right.right =  new TreeProblems(10);
        Set<Integer> nodesSet = new HashSet<>();
        printTopView(root, nodesSet, 0);
        compress();

    }
    private static void compress() {
        String test="aavvvqwqaa";
        int count=0,start=0,end=0;
        int length=test.length();
        for(int i=0;i<length-1;i++){
            if(test.charAt(i)!=test.charAt(i+1)){
                end=i;
                test=test+""+test.charAt(i)+( end-start+1);
                count=count+(end-start+1);
                start=end+1;
            }
        }
        System.out.println(test);
        if(count<length){
            test=test+test.charAt(count)+(length-count);

        }
        System.out.println(test.substring(length,test.length()));
    }

    private static void printTopView(TreeProblems root, Set<Integer> nodesSet, int horizontalDistance) {
        Queue<TreeProblems> queue = new LinkedList<>();
        queue.add(root);
        root.horizontalDistance = horizontalDistance;
        queue.add(null);
        while (true) {
            TreeProblems temp = queue.poll();
            if(temp == null) break;

            if(!nodesSet.contains(temp.horizontalDistance)) {
                System.out.print(temp.val+" ");
                nodesSet.add(temp.horizontalDistance);
            }
            if(temp.left!= null) {
                temp.left.horizontalDistance = temp.horizontalDistance-1;
                queue.add(temp.left);
            }

            if(temp.right!= null) {
                temp.right.horizontalDistance = temp.horizontalDistance+1;
                queue.add(temp.right);
            }
            if(queue.peek() == null) {
                queue.poll();
                queue.add(null);
            }
        }
    }
}
