package com.company;

import java.util.*;

/**
 * Created by Anmol on 2/1/2016.
 */
public class ReverseAlternateLevels {

    public static void main(String [] args) {
        Map<Integer, Stack<Integer>> map;
        TreeProblems root = TreeProblems.getBinaryTree();
        map = getAlternateLevelValues(root, 0);

    }

    private static  Map<Integer, Stack<Integer>> getAlternateLevelValues(TreeProblems root, int level) {
        Map<Integer, Stack<Integer>> map = new HashMap<>();
        Queue<TreeProblems> queue = new LinkedList<>();
        queue.add(root);
        queue.add(null);
        while(!queue.isEmpty()) {
            TreeProblems temp = queue.poll();
            if(temp == null && queue.isEmpty()) break;
            if(level%2 != 0) {
                if(map.containsKey(level)) {
                    Stack<Integer> integers = map.get(level);
                    integers.push(temp.val);
                    map.put(level, integers);
                } else {
                    Stack<Integer> st = new Stack<>();
                    st.push(temp.val);
                    map.put(level, st);
                }
            }
            if(temp.left!=null) {
                queue.add(temp.left);

            }

            if(temp.right!=null) {
                queue.add(temp.right);
            }
            if(queue.peek() == null) {
                level++;
                queue.poll();
                queue.add(null);
            }
        }
        return map;
    }
}
