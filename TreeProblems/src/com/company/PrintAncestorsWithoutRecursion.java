package com.company;

import java.util.Stack;

/**
 * Created by Anmol on 1/24/2016.
 */
public class PrintAncestorsWithoutRecursion {

    public static void main(String[] args) {
        postOrderTraversal(TreeProblems.getBinaryTree(), 8);
    }

    private static void postOrderTraversal(TreeProblems root, int val) {

        TreeProblems current = root;
        Stack<TreeProblems> stack = new Stack<TreeProblems>();
        if (current.right != null) {
            stack.push(current.right);
            stack.push(current);
            current = current.left;
        }
        while (!stack.isEmpty()) {
            if (current == null) {
                TreeProblems temp = stack.pop();
                if (!stack.empty() && temp.right == stack.peek()) {
                    current = temp;
                    temp = stack.pop();
                    stack.push(current);
                    current = temp;
                } else {
                    System.out.print(temp.val + " ");
                }
            } else {
                if (current.right != null) {
                    stack.push(current.right);
                    stack.push(current);
                    current = current.left;
                } else {

                        stack.push(current);
                        current = current.left;
                }
            }


        }

    }
}
