package com.company;

/**
 * Created by Anmol on 1/23/2016.
 */
public class BinaryTreeToDoublyLinkList {
    static DoubleLinkList head= null,next, prev;
    private static class DoubleLinkList {
        int val;
        DoubleLinkList next;
        DoubleLinkList prev;

        private DoubleLinkList(int val) {
            this.val = val;
        }
    }

    public static void main(String[] args) {

        treeToDoubleLL(TreeProblems.getRoot());

    }

    private static void treeToDoubleLL(TreeProblems root) {

        if(root == null) return;
        treeToDoubleLL(root.left);
        if(head == null){
            head = new DoubleLinkList(root.val);
            next = head;
        } else {
            next.next = new DoubleLinkList(root.val);
            prev = next.next;
            prev.prev = next;
            next = prev;
        }

        treeToDoubleLL(root.right);
    }
}
