package com.company;

/**
 * Created by Anmol on 2/1/2016.
 */
public class BinaryTreeToThreaded {
    public static void main(String []arg) {
        TreeProblems binaryTree = TreeProblems.getBinaryTree();
        binaryTreeToThreaded(binaryTree);
    }

    private static void binaryTreeToThreaded(TreeProblems root) {
        TreeProblems current = root;
        TreeProblems prev;
        while(current!=null) {
            if(current.left==null) {
                current = current.right;
            } else {
                prev = current.left;
                while(prev.right!=null && prev.right!=current) {
                    prev = prev.right;
                }
                if(prev.right == null) {
                    prev.right = current;
                    current = current.left;
                } else {
                    current = current.right;
                }
            }

        }
    }
}
