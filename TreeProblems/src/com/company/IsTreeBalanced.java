package com.company;

/**
 * Created by Anmol on 1/23/2016.
 */
public class IsTreeBalanced {

    public static void main(String[] args) {
        boolean isBalanced = isTreeBalanced(TreeProblems.getRoot());
        System.out.print(isBalanced);
    }

    private static boolean isTreeBalanced(TreeProblems root) {
        if (root == null) return true;
        int lHeight = heightOfTree(root.left) + 1;
        int rHeight = heightOfTree(root.right) + 1;
        System.out.println("LHeight: " + lHeight + "RHeight: " + rHeight);
        return Math.abs(lHeight - rHeight) <= 1 && isTreeBalanced(root.left) && isTreeBalanced(root.right);
    }

    private static int heightOfTree(TreeProblems root) {
        if (root == null) {
            return 0;
        }
        int lHeight = heightOfTree(root.left);
        int rHeight = heightOfTree(root.right);
        return (Math.max(lHeight, rHeight) + 1);
    }
}
