package com.company;

/**
 * Created by Anmol on 2/13/2016.
 */
public class RemoveAllHalfNodes {
    public static void main(String[] args) {
        TreeProblems root = removeHalfNodes(TreeProblems.getRoot());

    }

    private static TreeProblems removeHalfNodes(TreeProblems root) {
        if(root == null) return null;

        root.left = removeHalfNodes(root.left);
        root.right = removeHalfNodes(root.right);

        if(root.left == null && root.right == null) return root;

        if(root.left == null) {
            TreeProblems right = root.right;
            return right;
        }
        if(root.right == null) {
            TreeProblems left = root.left;
            return left;
        }
        return root;
    }
}
