package com.company;

/**
 * Created by Anmol on 1/20/2016.
 */
public class MorrisTraversal {

    public static void main(String[] args) {
        morrisTraversal(TreeProblems.getRoot());
    }
    static void morrisTraversal(TreeProblems root) {

        TreeProblems current = root, pre;
        while (current!=null) {
            if(current.left==null) {
                System.out.print(current.val + " ");
                current = current.right;
            } else {
                pre = current.left;
                while(pre.right!=null&& pre.right!=current) {
                    pre = pre.right;
                }
                if(pre.right == null ) {
                    pre.right= current;
                    current = current.left;

                } else {
                    System.out.print(current.val+" ");
                    pre.right=null;
                    current = current.right;
                }

            }
        }

    }

}
