package com.company;

import java.util.Arrays;

/**
 * Created by Anmol on 1/24/2016.
 */class Node1 {

    String dataString;
    Node1 left;
    Node1 right;

    Node1(String dataString) {
        this.dataString = dataString;
        //Be default left and right child are null.
    }

    public String getDataString() {
        return dataString;
    }
}

public class FBQuestion {

    // Method to create a binary tree which stores all interpretations
    // of arr[] in lead nodes
    public static Node1 createTree(int data, String pString, int[] arr) {

        pString = pString+alphabet[data];

        Node1 root = new Node1(pString);
        if(arr.length == 0) return root;

        data = arr[0];

        int[] copiedArr = Arrays.copyOfRange(arr, 1, arr.length);
        root.left = createTree(data, pString, copiedArr);

        if(arr.length>=2) {
            data = arr[0]*10 + arr[1];
            if(data<=26) {
                int[] newArr = Arrays.copyOfRange(arr, 2, arr.length);
                root.right = createTree(data, pString, newArr);
            }
        }
        return root;
    }

    // To print out leaf nodes
    public static void printleaf(Node1 root) {
        if (root == null)
            return;

        if (root.left == null && root.right == null)
            System.out.print(root.getDataString() + "  ");

        printleaf(root.left);
        printleaf(root.right);
    }

    // The main function that prints all interpretations of array
    static void printAllInterpretations(int[] arr) {

        // Step 1: Create Tree
        Node1 root = createTree(0, "", arr);

        // Step 2: Print Leaf nodes
        printleaf(root);

        System.out.println();  // Print new line
    }

    // For simplicity I am taking it as string array. Char Array will save space
    private static final String[] alphabet = {"", "a", "b", "c", "d", "e",
            "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r",
            "s", "t", "u", "v", "w", "x", "v", "z"};

    // Driver method to test above methods
    public static void main(String args[]) {

        // aacd(1,1,3,4) amd(1,13,4) kcd(11,3,4)
        // Note : 1,1,34 is not valid as we don't have values corresponding
        // to 34 in alphabet
        int[] arr = {1, 1, 3, 4};
        printAllInterpretations(arr);
        int[] arr5 = {1, 0};
        printAllInterpretations(arr5);
        /*// aaa(1,1,1) ak(1,11) ka(11,1)
        int[] arr2 = {1, 1, 1};
        printAllInterpretations(arr2);

        // bf(2,6) z(26)
        int[] arr3 = {2, 6};
        printAllInterpretations(arr3);

        // ab(1,2), l(12)
        int[] arr4 = {1, 2};
        printAllInterpretations(arr4);

        // a(1,0} j(10)
        int[] arr5 = {1, 0};
        printAllInterpretations(arr5);

        // "" empty string output as array is empty
        int[] arr6 = {};
        printAllInterpretations(arr6);

        // abba abu ava lba lu
        int[] arr7 = {1, 2, 2, 1};
        printAllInterpretations(arr7);*/
    }
}
