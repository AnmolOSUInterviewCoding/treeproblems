package com.company;

import java.util.Map;

/**
 * Created by Anmol on 1/30/2016.
 */
public class DiameterOfTree {

    static class Height{
        int height;
    }
    public static void main(String[] args) {
        int diameter = diameter(TreeProblems.getBinaryTree(), new Height());
    }

    private static int diameter(TreeProblems root, Height height) {
        if(root == null){
            height.height = 0;
            return 0;
        }
        Height lh = new Height();
        Height rh = new Height();
        lh.height++;
        rh.height++;
        int lDia = diameter(root.left,lh);
        int rDia = diameter(root.right,rh);

        height.height = Math.max(lh.height, rh.height) + 1;
        return Math.max(Math.max(lDia, rDia), lh.height + rh.height + 1);
    }
}
