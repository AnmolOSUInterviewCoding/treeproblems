package com.company;

/**
 * Created by Anmol on 1/31/2016.
 */
public class LeafNodesToDLL {
    static DoublyLinkedList head = null;
    static DoublyLinkedList temp = head;
    static class DoublyLinkedList {
        int val;
        DoublyLinkedList prev;
        DoublyLinkedList next;

        public DoublyLinkedList(int val) {
            this.val = val;
            prev = null;
            next = null;
        }
    }

    public static void main(String[] args) {


        TreeProblems binaryTree = TreeProblems.getBinaryTree();
        extractLeavesToDLL(binaryTree);
    }

    private static TreeProblems extractLeavesToDLL(TreeProblems root) {
        if (root == null) {
            return null;
        }
        if (root.left == null && root.right == null) {
            if (head == null) {
                head = new DoublyLinkedList(root.val);
                temp = head;
            } else {
                DoublyLinkedList temp1 = new DoublyLinkedList(root.val);
                temp.next = temp1;
                temp1.prev = temp;
                temp = temp1;
            }
            return null;
        }


        root.left = extractLeavesToDLL(root.left);
        root.right = extractLeavesToDLL(root.right);
        return root;
    }

}
