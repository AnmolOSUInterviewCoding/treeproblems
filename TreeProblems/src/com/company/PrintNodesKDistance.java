package com.company;

/**
 * Created by Anmol on 1/22/2016.
 */
public class PrintNodesKDistance {

    public static void main(String[] args) {
        printKDistanceNodes(TreeProblems.getRoot(), 2, 0);
    }

    private static void printKDistanceNodes(TreeProblems root, int distance, int level) {

        if(root == null) return;

        if(level == distance) {
            System.out.print(root.val + " ");
            return;
        }
        printKDistanceNodes(root.left, distance, level+1);
        printKDistanceNodes(root.right, distance, level+1);

    }

}
