package com.company;

/**
 * Created by Anmol on 1/25/2016.
 */
public class BinaryTreeToLinkList {
    static SortedLinkListToBST.LinkedListProblems next = null;
    static SortedLinkListToBST.LinkedListProblems head = null;

    public static void main(String[] agrs) {
        getLinkList(TreeProblems.getRoot());
        System.out.print("LinkList : ");
    }

    private static void getLinkList(TreeProblems root) {
        if(root == null) return;

        getLinkList(root.left);
        if(head == null) {
            head = new SortedLinkListToBST.LinkedListProblems(root.val);
            next = head;
        } else {
            next.next = new SortedLinkListToBST.LinkedListProblems(root.val);
            next = next.next;
        }
        getLinkList(root.right);
    }

}
