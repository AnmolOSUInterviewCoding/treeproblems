package com.company;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Anmol on 1/22/2016.
 */
public class ConnectNodesAtSameLevel {

    public static void main(String[] args) {
        TreeProblems root = new TreeProblems(1);
        root.left = new TreeProblems(2);
        root.right = new TreeProblems(3);
        root.left.left = new TreeProblems(4);
        root.right.right = new TreeProblems(5);
        setNextRight(root);
        System.out.print("Set next right done");
    }

    private static void setNextRight(TreeProblems root) {
        root.nextRight = null;
        Queue<TreeProblems> queue = new LinkedList<TreeProblems>();
        queue.add(root);
        queue.add(null);
        while (!queue.isEmpty()) {
            TreeProblems parent = queue.poll();
            if(parent==null) break;
            if(parent.left!=null) {
                queue.add(parent.left);
                parent.left.nextRight = parent.right !=null? parent.right : (parent.nextRight.left!=null? parent.nextRight.left : parent.nextRight.right);
            }
            if(parent.right!=null) {

                queue.add(parent.right);
                if(parent.nextRight== null) {
                    parent.right.nextRight =null;
                } else {
                    parent.right.nextRight = parent.nextRight.left!=null?parent.nextRight.left : parent.nextRight.right;
                }
            }
            if(queue.peek() == null) {
                queue.poll();
                queue.add(null);
            }
        }
    }

}
