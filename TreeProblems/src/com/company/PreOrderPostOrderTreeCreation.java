package com.company;

/**
 * Created by Anmol on 1/23/2016.
 */
public class PreOrderPostOrderTreeCreation {

    public static void main(String[] args) {
        int[] preOrder = new int[]{1, 2, 4, 8, 9, 5, 3, 6, 7};
        int[] postOrder = new int[]{8, 9, 4, 5, 2, 6, 7, 3, 1};

        TreeProblems root = createTree(preOrder, postOrder, 0, 0, preOrder.length - 1);


    }

    private static TreeProblems createTree(int[] preOrder, int[] postOrder,int preIndex, int start, int end) {

        if (preIndex >= preOrder.length || start > end) return null;
        TreeProblems root = new TreeProblems(preOrder[preIndex++]);
        if (start == end) {
            return root;
        }

        int index = find(postOrder, preOrder[preIndex]);
        if (index <= end) {
            root.left = createTree(preOrder, postOrder, preIndex,start, index);
            root.right = createTree(preOrder, postOrder,preIndex, index + 1, end);
        }
        return root;
    }

    private static int find(int[] postOrder, int key) {
        int i = 0;
        while (postOrder[i] != key) {
            i++;
        }
        return i;
    }

}
