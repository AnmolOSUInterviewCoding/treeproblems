package com.company;

import java.util.Stack;

/**
 * Created by Anmol on 1/31/2016.
 */
public class PostOrderTraversal {

    public static void main(String[] args) {
        printPostOrder(TreeProblems.getBinaryTree());
    }

    private static void printPostOrder(TreeProblems root) {
        Stack<TreeProblems> stack = new Stack<>();
        while (true) {
            if (root == null) {
                if (stack.isEmpty()) break;
                TreeProblems temp = stack.pop();
                if (!stack.empty() && temp.right == stack.peek()) {
                    root = stack.pop();
                    stack.push(temp);
                } else {
                    System.out.print(temp.val + " ");
                }
            } else if (root.right != null) {
                stack.push(root.right);
                stack.push(root);
                root = root.left;
            } else {
                stack.push(root);
                root = root.left;
            }
        }
    }
}
