package com.company;

/**
 * Created by Anmol on 1/31/2016.
 */
public class DoubleTree {

    public static void main(String[] agrs) {
        TreeProblems root = new TreeProblems(2);
        root.left = new TreeProblems(1);
        root.right = new TreeProblems(3);
        getDoubleTree(root);
    }

    private static void getDoubleTree(TreeProblems root) {
        if(root == null) return;

        getDoubleTree(root.left);
        getDoubleTree(root.right);
        TreeProblems left = root.left;
        root.left = new TreeProblems(root.val);
        root.left.left = left;

    }

}
