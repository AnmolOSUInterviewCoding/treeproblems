package com.company;

/**
 * Created by Anmol on 1/23/2016.
 */
public class PreOrderMorrisTraversal {

    public static void main(String[] args) {
        preOrderMorrisTraversal(TreeProblems.getRoot());
    }

    private static void preOrderMorrisTraversal(TreeProblems root) {

        TreeProblems current = root, prev;
        while(current!=null) {
            if(current.left == null) {
                System.out.print(current.val + " ");
                current = current.right;
            } else {
                prev = current.left;
                while(prev.right!=null && prev.right != current) {
                    prev = prev.right;
                }
                if(prev.right == null) {
                    prev.right = current;
                    System.out.print(current.val + " ");
                    current = current.left;
                } else {
                    prev.right = null;
                    current = current.right;
                }
            }
        }

    }

}
