package com.company;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Anmol on 1/31/2016.
 */
public class VerticalSumOfTree {

    public static void main(String[] agrs) {
        Map<Integer, Integer> verticalSum = new HashMap<>();
        getVerticalSum(TreeProblems.getBinaryTree(), 0, verticalSum);
        for(Map.Entry<Integer, Integer> entry : verticalSum.entrySet()) {
            System.out.println("Vertical Level: "+ entry.getKey() + "Sum: " + entry.getValue());
        }
    }

    private static void getVerticalSum(TreeProblems root, int level, Map<Integer, Integer> verticalSum) {
        if(root == null) return;
        if(verticalSum.containsKey(level)) {
            verticalSum.put(level, verticalSum.get(level)+ root.val);
        } else {
            verticalSum.put(level, root.val);
        }
        getVerticalSum(root.left, level-1, verticalSum);
        getVerticalSum(root.right, level+1, verticalSum);
    }

}
