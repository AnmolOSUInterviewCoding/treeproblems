package com.company;

/**
 * Created by Anmol on 1/25/2016.
 */
public class BinaryTreeLowestCommonAncestor {

    public static void main(String[] args) {
        TreeProblems root = TreeProblems.getBinaryTree();
        TreeProblems lca = lowestCommonAncestor(root, 5, 0);
        System.out.print(lca.val);
    }

    private static TreeProblems lowestCommonAncestor(TreeProblems root, int val1, int val2) {
        if(root == null) return null;
        if(root.val == val1 || root.val == val2) {
            return root;
        }
        TreeProblems leftLca = lowestCommonAncestor(root.left, val1, val2);
        TreeProblems rightLca = lowestCommonAncestor(root.right, val1, val2);

        if(leftLca!=null && rightLca!=null) return root;

        return leftLca!=null ? leftLca : rightLca;
    }

}
