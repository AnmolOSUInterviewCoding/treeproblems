package com.company;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Anmol on 2/12/2016.
 */
public class SumMultiplicationOfLeafLevelNodesGeeks {

    public static void main(String[] agrs) {
        TreeProblems root = TreeProblems.getRoot();
        HashMap<Integer, Integer> map = new HashMap<>();
        fillMap(root,0,map);
        int sum = 1;
        for(Map.Entry<Integer, Integer> entry : map.entrySet()) {
            sum = sum*entry.getValue();
        }
        System.out.print(sum);
    }

    private static void fillMap(TreeProblems root, int level, HashMap<Integer, Integer> map) {
        if(root == null) return;

        if(root.left == null && root.right == null) {
            if(map.containsKey(level)) {
                map.put(level, map.get(level)+ root.val);

            } else {
                map.put(level, root.val);
            }
            return;
        }
        fillMap(root.left, level+1, map);
        fillMap(root.right, level+1, map);
    }
}
