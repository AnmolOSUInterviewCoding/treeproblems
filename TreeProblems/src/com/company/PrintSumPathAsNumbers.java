package com.company;

/**
 * Created by Anmol on 1/25/2016.
 */
public class PrintSumPathAsNumbers {
    public static void main(String[] args) {
        printNumber(TreeProblems.getRoot(), 0);

    }

    private static void printNumber(TreeProblems root, int sum) {
        if(root == null) return;

        sum= sum*10 + root.val;
        printNumber(root.left, sum);
        printNumber(root.right, sum);
        if(root.left == null&&root.right==null) {
            System.out.print(sum +" ");
        }
    }

}
