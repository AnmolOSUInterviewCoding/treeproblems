package com.company;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Anmol on 1/20/2016.
 */
public class LevelOrderTraversal {

    public static void main(String[] args) {

        levelOrderTraversal(TreeProblems.getRoot());

    }

    private static void levelOrderTraversal(TreeProblems root) {

        Queue<TreeProblems> queue = new LinkedList<TreeProblems>();

        queue.add(root);
        queue.add(null);
        TreeProblems temp;
        while(!queue.isEmpty()) {

            temp = queue.poll();
            if(temp == null) break;
            System.out.println(temp.val);
            if(temp.left!=null) queue.add(temp.left);
            if(temp.right!=null) queue.add(temp.right);
            if(queue.peek()== null) {
                queue.poll();
                System.out.println("One Level Complete");
                queue.add(null);
            }
        }



    }

}
