package com.company;

import sun.reflect.generics.tree.Tree;

/**
 * Created by Anmol on 1/22/2016.
 */
public class CreateTreeFromInOrderPreOrder {
    static int preOrderIndex;
    public static void main(String[] args) {
        int[] inOrder = new int[]{1,2,3,4,5,6,8};
        int[] preOrder = new int[]{4,2,1,3,5,6,8};

        TreeProblems root = createTree(inOrder, preOrder, 0, inOrder.length-1);
    }

    private static TreeProblems createTree(int[] inOrder, int[] preOrder, int start, int end)  {
        if(start > end) {
            return null;
        }

        int rootVal = preOrder[preOrderIndex++];
        TreeProblems root = new TreeProblems(rootVal);
        int inOrderIndex = findInorderIndex(inOrder, rootVal);
        if(start == end) return root;

        root.left = createTree(inOrder, preOrder,  start, inOrderIndex-1);
        root.right = createTree(inOrder, preOrder, inOrderIndex+1, end);
        return root;

    }

    private static int findInorderIndex(int[] inOrder, int rootVal) {
        int i =0;
        while(inOrder[i]!=rootVal) {
            i++;
        }
        return i;
    }
}
