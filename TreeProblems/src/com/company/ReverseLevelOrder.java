package com.company;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * Created by Anmol on 1/31/2016.
 */
public class ReverseLevelOrder {

    public static void main(String[] args) {
        reverseLevelOrder(TreeProblems.getBinaryTree());
    }

    private static void reverseLevelOrder(TreeProblems root) {
        Queue<TreeProblems> queue = new LinkedList<>();
        queue.add(root);
        queue.add(null);
        Stack<TreeProblems> stack = new Stack<>();
        while(true) {
            if(queue.isEmpty()) break;
            TreeProblems temp = queue.poll();
            if(temp == null)break;;
            stack.push(temp);

            if(temp.right!=null) {
                queue.add(temp.right);
            }
            if(temp.left!=null) {
                queue.add(temp.left);
            }
            if(queue.peek() == null) {
                queue.poll();
                queue.add(null);
            }
        }
        while(!stack.empty()) {
            System.out.print(stack.pop().val + " ");
        }
    }

}
