import java.util.ArrayList;

/**
 * Created by Anmol on 1/11/2016.
 */
public class UglyNumbers {

    public static void main(String args[]) {

        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(2);
        list.add(3);
        list.add(5);

        for(int i= 4;list.size()<100;i++) {
              if(i%2 == 0){
                  if(list.contains(i/2)) {
                      list.add(i);
                      continue;
                  }
              }
            if(i%3 == 0){
                if(list.contains(i/3)) {
                    list.add(i);
                    continue;
                }
            }
            if(i%5 == 0){
                if(list.contains(i/5)) {
                    list.add(i);
                }
            }
        }
        for(int i : list) {
            System.out.println(i);
        }
    }

}
