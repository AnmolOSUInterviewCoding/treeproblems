/**
 * Created by Anmol on 1/14/2016.
 */
public class HouseRobber2 {

    public static void main(String[] args) {
        int[] arr = new int[]{12,3,4,55,10,23,24};

        int[] resultFirst = new int[arr.length];
        int[] resultLast = new int[arr.length];
        resultFirst[0] = 0;
        resultFirst[1] = arr[0];
        for(int i =2;i<arr.length;i++) {
            resultFirst[i] = Math.max(resultFirst[i-2] + arr[i-1], resultFirst[i-1]);
        }
        for(int i : resultFirst) {
            System.out.println(i);
        }
        resultLast[0] = 0;
        resultLast[1] = arr[1];
        for(int i =2;i<arr.length;i++) {
            resultLast[i] = Math.max(resultLast[i-2] + arr[i], resultLast[i-1]);
        }
        for(int i : resultLast) {
           System.out.println(i);
        }

    }

}
