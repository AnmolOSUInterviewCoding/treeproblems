/**
 * Created by Anmol on 1/11/2016.
 */
public class Problem1 {

    public static void main(String[] args) {

//        int[] arr = new int[]{2, 2, 2, 2, 3, 4, 5, 6, -1, 2, 3};
//        int[] arr = new int[]{2, 2, 2, 2, 1, 2, -1, 2, 1, 3};
        int[] arr = new int[]{10,40,20,30,50,60};
        int max = 1;
        int startingIndex = 0;
        int index = 0;
        for (int i = 1; i < arr.length; i++) {
            if(arr[i] > arr[i-1]) {
                if(i-startingIndex +1 > max) {
                    index = startingIndex;
                    max = i-startingIndex+1;
                }
            }
            else {
                startingIndex = i;
            }
        }
        System.out.print(index);


    }

}
