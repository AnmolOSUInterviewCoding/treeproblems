import java.math.BigInteger;

/**
 * Created by Anmol on 1/25/2016.
 */
public class ModifiedFibbonaci {
    public static void main(String[] args) {
        BigInteger a = new BigInteger("1");
        BigInteger b = new BigInteger("2");
        int n = 20;
        BigInteger c = new BigInteger("0");
        for(int i =2; i<n;i++) {
            c = a.add(b.multiply(b));
            a = b;
            b = c;

        }
        System.out.print(c);
    }
}
