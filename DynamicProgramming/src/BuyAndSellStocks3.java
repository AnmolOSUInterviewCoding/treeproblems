import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Anmol on 1/17/2016.
 */
public class BuyAndSellStocks3 {

    public static void main(String[] args) {
//        int[] prices = new int[]{3,4,1,5,6,3,9};
//        int[] prices = new int[]{3,2,6,5,0,3};
        int[] prices = new int[]{1, 2, 4, 2, 5, 7, 2, 4, 9, 0};
        int transactions = 2;
        int[][] profit = new int[transactions + 1][prices.length];


        for (int i = 1; i <= transactions; i++) {
            for (int j = 1; j < prices.length; j++) {

                int maxVal = profit[i][j-1];
                for(int m = 0; m <j; m++) {
                    maxVal = Math.max(maxVal, prices[j] - prices[m] + profit[i-1][m]);
                }
                profit[i][j] = maxVal;
            }

        }
        System.out.print(profit[transactions][prices.length-1]);
    }

}
