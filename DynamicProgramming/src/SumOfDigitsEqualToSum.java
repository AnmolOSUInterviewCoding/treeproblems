/**
 * Created by Anmol on 1/27/2016.
 */
public class SumOfDigitsEqualToSum {

    public static void main(String[] args) {
        int n = 3;
        int sum = 5;
        int limit = (int) Math.pow(10, n);
        int count =0;

        int[][] result = new int[n+1][limit+1];
        for(int i = 0;i<=10;i++) {
            result[1][i] = i;
        }
        for(int i =2; i<=n;i++) {
            limit = (int) Math.pow(10, i);
            for(int j = (i-1)*10;j<limit;j++) {
                result[i][j] = result[i-1][j/10] + j%10;

            }
        }
        for(int i = 100;i<=1000;i++) {
            if(result[3][i] == sum) {
                count++;
            }
        }
        System.out.print(count);
    }

}
