/**
 * Created by Anmol on 1/12/2016.
 */
public class MaxProductSubArray {

    public static void main(String[] args) {
//        int[] nums = new int[]{-2,0,-1};
//        int[] nums = new int[]{-4,3,-9,5,-2};
        int[] nums = new int[]{-1,-2,-9,-6};
//        int[] nums = new int[]{2,3,-2,4};
        int len = nums.length;
//        if(len == 0) return 1;
//        if( len == 1) return nums[0];
        int maxEndHere = 1;
        int minEndHere = 1;
        int maxSoFar = 0;

        for(int i = 0; i<nums.length;i++) {
            if(nums[i]>0) {
                if(minEndHere == 0){
                    minEndHere = 1;
                    maxEndHere = 1;
                }
                else if(minEndHere< 0 && maxEndHere == 0) {
                    maxEndHere = 1;
                }
                maxEndHere = maxEndHere*nums[i];
                minEndHere = Math.min(minEndHere* nums[i], 1);
            } else if(nums[i]<0) {

                maxEndHere = Math.max(minEndHere*nums[i], 0);
                minEndHere = (maxEndHere == 0? 1 : maxEndHere)*minEndHere*nums[i];

            } else {
                minEndHere = 0;
                if(i!=nums.length-1) {
                    maxEndHere = 0;
                }
            }
            if(maxSoFar<maxEndHere) {
                maxSoFar = maxEndHere;
            }
        }
    }
}
