/**
 * Created by Anmol on 1/13/2016.
 */
public class MinPathLeetCode {

    public static void main(String[] args) {
        int[][] grid = { {1,3,1}, {1,5,1},{4,2,1}};

        int row = grid.length;
        int col = grid[0].length;

        int[][] result = new int[row+1][col+1];

        for(int i = 1; i<=row;i++) {
            for(int j = 1; j<=col;j++) {
                if(i>1 && j==1) {
                    result[i][j] = grid[i-1][j-1] + result[i-1][j];
                } else if(i==1&& j>1) {
                    result[i][j] = grid[i-1][j-1] + result[i][j-1];
                }
                else
                result[i][j] = grid[i - 1][j - 1] + Math.min(result[i - 1][j], result[i][j - 1]);
            }
        }
        for(int i =0; i<result.length;i++) {
            System.out.println();
            for(int j = 0; j<result[0].length;j++) {
                System.out.print("\t" + result[i][j]);
            }
        }
        System.out.println("\t"+result[row][col]);
    }
    
}
