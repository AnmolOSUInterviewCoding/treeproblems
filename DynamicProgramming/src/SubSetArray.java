import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Anmol on 1/10/2016.
 */
public class SubSetArray {

    public static void main(String []args) {
//        int arr[] = new int[] {5,1,0,3,4,6,2};
        Random random = new Random();
//        int []arr  = new int[1000];
        List<Integer> list = new ArrayList<Integer>();
        for(int i = 0; i<10;i++) {
            int number = random.nextInt(10);
            if(!list.contains(number)) {
                list.add(number);
            }
            else {
                i--;
            }
        }
//        int arr[] = new int[] {1,4,5,0,2,3};
//        int[] arr = list.toArray();
        int[] arr = new int[10];
        int j = 0;
        for(int i : list) {
            arr[j] = i;
            System.out.println(i);
            j++;
        }
        int[] result = new int[arr.length];
        int maxSet = 0;
        for (int i = 0; i <arr.length; ++ i)
        {
            if (result [i] == 0)
            {
                result [i] = sizeCount (arr, result, i, arr.length, i);
                if (result [i]> maxSet)
                {
                    maxSet = result [i];
                }
            } // Pruning-if M [i]! = 0 then M [i] <= maxSet
        }
        System.out.println("Maxset: " + maxSet);
    }

    private static int sizeCount(int[] arr, int[] result, int i, int length, int startingIndex) {
        int count = 1;
        if (arr [i] != startingIndex)
        {
            if (result [arr [i]] == 0)
            {
                count = 1 + sizeCount (arr, result, arr [i], length, startingIndex);
            } else // pruning
            {
                count = 1 + result [arr [i]];
            }
        }
        result [i] = count;
        return result [i];
    }

}
