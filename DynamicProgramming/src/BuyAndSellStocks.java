/**
 * Created by Anmol on 1/15/2016.
 */
public class BuyAndSellStocks {

    public static void main(String[] args) {
        int[] prices = new int[]{3,4,1,5,6,3,9};
        int profit = 0;
        int buyPrice = prices[0];
        for(int i =1; i< prices.length; i++) {
            if(prices[i]> buyPrice && (prices[i]- buyPrice > profit)) {
                profit = prices[i] - buyPrice;
            } else if(buyPrice > prices[i]){
                buyPrice = prices[i];
            }
        }
        System.out.print(profit);
    }

}
