import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anmol on 2/12/2016.
 */
public class TraingleSumLeetCode {
    public static void main(String[] args) {
        List<List<Integer>> triangle = new ArrayList<List<Integer>>();
        List<Integer> l1 = new ArrayList<Integer>();
        l1.add(-1);
        triangle.add(l1);
        List<Integer> l2 = new ArrayList<Integer>();
        l2.add(2);
        l2.add(3);
        triangle.add(l2);
        List<Integer> l3 = new ArrayList<Integer>();
        l3.add(1);
        l3.add(-1);
        l3.add(-3);
        triangle.add(l3);
        List<Integer> l4 = new ArrayList<Integer>();
        l4.add(-6);
        l4.add(1);
        l4.add(1);
        l4.add(1);
        triangle.add(l4);

        int[] result = new int[triangle.size()];

        for(int i = 0;i<triangle.size();i++) {
            result[i] = triangle.get(triangle.size()-1).get(i);
        }

        for(int i = triangle.size()-2; i>=0;i--) {
            for(int j = 0;j<triangle.get(i).size();j++) {
                result[j] =triangle.get(i).get(j) + Math.min(result[j], result[j+1]);
            }
        }
    }
}
