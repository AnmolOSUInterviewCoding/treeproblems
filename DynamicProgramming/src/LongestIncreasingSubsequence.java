import java.util.ArrayList;

/**
 * Created by Anmol on 1/9/2016.
 */
public class LongestIncreasingSubsequence {

    public static void main(String args[]) {
        int[] arr = new int[] { 10, 22, 9, 33, 21, 50, 41, 60, 80 };
//        int[] arr = new int[] { 1,3,6,7,9,4,10,5,6,7,8,9,10,11 };
        /*int[] result = new int[arr.length];
        result[0] = 1;
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(arr[0]);
        for(int i =0;i <arr.length;i++ ) {
            result[i] = 1;
        }
        for(int i = 1;i<arr.length; i++) {
            for (int j = 0; j < i; j++) {
                if (arr[i] > arr[j] && result[i] < result[j] + 1) {
                    result[i] = result[j] + 1;
//                    list.add(arr[i]);
                }
            }
        }
        int max = 0;
        for(int i : result) {
            System.out.print(i + " ");
            if(max<i) max = i;
        }


//        return T[maxIndex];
        System.out.println(" Max: " + max);
        *//*for(int number : list) {
            System.out.println(number);
        }*/
        int T[] = new int[arr.length];
        int actualSolution[] = new int[arr.length];
        for(int i=0; i < arr.length; i++){
            T[i] = 1;
            actualSolution[i] = i;
        }

        for(int i=1; i < arr.length; i++){
            for(int j=0; j < i; j++){
                if(arr[i] > arr[j]){
                    if(T[j] + 1 > T[i]){
                        T[i] = T[j] + 1;
                        //set the actualSolution to point to guy before me
                        actualSolution[i] = j;
                    }
                }
            }
        }

        //find the index of max number in T
        int maxIndex = 0;
        for(int i=0; i < T.length; i++){
            if(T[i] > T[maxIndex]){
                maxIndex = i;
            }
        }

        //lets print the actual solution
        int t = maxIndex;
        int newT = maxIndex;
        do{
            t = newT;
            System.out.print(arr[t] + " ");
            newT = actualSolution[t];
        }while(t != newT);
        System.out.println();


        int[] result = new int[arr.length];
        for(int i = 0;i<arr.length;i++) {
            result[i] = 1;
        }

        for(int i = arr.length-2;i>=0;i--) {
            for(int j = arr.length-1;j>i;j--) {
                if(arr[j] > arr[i] && result[j] + 1 > result[i]) {
                    result[i] = 1+ result[j];
                }
            }
        }
        for(int i : result){
            System.out.print(i + " ");
        }
    }

}





