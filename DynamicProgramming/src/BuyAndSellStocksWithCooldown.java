/**
 * Created by Anmol on 1/15/2016.
 */
public class BuyAndSellStocksWithCooldown {

    public static void main(String[] args) {
//        int[] prices = new int[]{3,2,6,5,0,3};
        int[] prices = new int[]{3, 4, 1, 5, 6, 3, 9};
//        int[] prices = new int[]{1,4,2};
//        [6,1,3,2,4,7]
        /*int profit = 0;

        int buyPrice = prices[0];
        int cooldown = 0;
        int result[] = new int[prices.length];

        for (int i = 1; i < prices.length; i++) {

            if (cooldown >= 2 && cooldown == i - 1 && prices[cooldown] < prices[i]) {
                if (result[cooldown - 2] + (prices[i] - prices[cooldown]) > result[i]) {
                    result[i] = result[cooldown - 2] + prices[i] - prices[cooldown];
                    buyPrice = prices[cooldown];
                    cooldown = i + 1;
                } else {
                    result[i] = result[i - 1];
                }
            } else {
                if (prices[i] > buyPrice && (prices[i] - buyPrice > result[i])) {
                    if(i == cooldown && prices[i] < prices[i-1]) {
                        result[i] = result[i-1];
                    }else if(i==cooldown) {
                        result[i] =  prices[i] - buyPrice;
                        cooldown = i + 1;
                    }
                    else {
                        result[i] = prices[i] - buyPrice + result[i-1];
                        cooldown = i + 1;
                    }
                } else if (buyPrice > prices[i]) {
                    buyPrice = prices[i];
                    result[i] = result[i - 1];
                } else {
                    result[i] = result[i - 1];
                }
            }
        }*/
        int totalIncome = 0;

        int soldPrice;
        int boughtPrice = prices[0];
        int numberOfStocks = 1;
        int []boughtStocks = new int[prices.length];

        for(int i = 1; i < prices.length; i++)  {
            int curStockPrice = prices[i];
            if(curStockPrice - boughtPrice > boughtStocks[numberOfStocks])   { // > soldPrice of boughtStock
                soldPrice = curStockPrice - boughtPrice;
                boughtStocks[numberOfStocks] = soldPrice;
            }else if(curStockPrice < boughtPrice || boughtStocks[numberOfStocks] != 0)   {
                boughtPrice = curStockPrice;
                if(i < prices.length-1)
                    numberOfStocks++;
            }
        }

        for (int boughtStock : boughtStocks) {
            totalIncome += boughtStock;
        }

        System.out.print(totalIncome);
    }

}
