/**
 * Created by Anmol on 2/7/2016.
 */
public class SlidingWindowMaximumLeetCode {
    public static void main(String[] args) {
        int[] nums = new int[]{1,-1};
        int k =1;
        if(nums.length == 0 );
        int max = 0;;
        int[] result = new int[nums.length - k +1];
        if(nums.length ==1 );
        for(int i = 0; i<nums.length; i++) {
            if(i<k-1) continue;
            if(i == k-1) {
                max = getMax(nums, k, i-k+1);
                result[i-k+1] = max;
            } else {
                if(nums[i] == max) {
                    max = Math.max(max, nums[i]);
                } else {
                    max = getMax(nums, k, i-k+1);
                }
                result[i-k+1] = max;
            }

        }

    }
    public static int getMax(int[] nums,int k, int start) {
        int m = Integer.MIN_VALUE;
        int i;
        for(i = start; i< k; i++) {
            if(m< nums[i]) {
                m = nums[i];
            }
        }
        return Math.max(m, nums[i-1]);
    }
}
