/**
 * Created by Anmol on 1/16/2016.
 */
public class BuyAndSellStocks2 {

    public static void main(String[] args) {
        int[] prices = new int[]{3,4,1,5,6,3,9};
//        int[] prices = new int[]{3,2,6,5,0,3};
        int totalProfit = 0;
        int maxProfitTillHere = 0;
//        int[] profit = new int[prices.length +1];
//        profit[0] = 0;
        int buyPrice = prices[0];
        for(int i = 1; i<prices.length; i++) {
            if(prices[i]>buyPrice && prices[i] - buyPrice >maxProfitTillHere) {
                maxProfitTillHere = prices[i] - buyPrice;
            } else if(buyPrice > prices[i] || prices[i] < prices[i-1]) {
                buyPrice = prices[i];
                totalProfit += maxProfitTillHere;
                maxProfitTillHere = 0;
            }
        }
        totalProfit+= maxProfitTillHere;
        System.out.print(totalProfit);

    }

}
