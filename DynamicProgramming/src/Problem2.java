import java.util.Iterator;

/**
 * Created by Anmol on 1/11/2016.
 */
public class Problem2 {

    public static void main(String[] args) {

        String S = "23:58:59";
        String T = "00:01:02";

        TimeIterable timeIterable = new TimeIterable(S,T);
        int count = 0;
        /*int hours,seconds,minutes;
        String[] strings = S.split(":");
        hours = Integer.parseInt(strings[0]);
        minutes = Integer.parseInt(strings[1]);
        seconds = Integer.parseInt(strings[2]);*/
        Iterator iterator = timeIterable.iterator();
        while (iterator.hasNext()) {
            if((Boolean) iterator.next()){
                count++;
            }
        }
      /*  while(!S.equals(T)) {
            if(seconds<59){
                seconds++;
            } else {
                seconds = 0;
                if(minutes<59) {
                    minutes++;

                } else {
                    minutes = 0;
                    if(hours<23) {
                        hours++;
                    } else {
                        hours = 0;
                    }
                }
            }
            boolean interesting = isInteresting(S);
            S = getTime(hours, minutes, seconds);
            if(interesting) count++;
        }*/
        if(isInteresting(T)) count++;
        System.out.print(count);


    }


    public static boolean isInteresting(String S) {
        int flag[] = new int[10];
        String[] strings = S.split(":");
        for(String st: strings) {
            int count = 0;
            int i = Integer.parseInt(st);
            do{
                flag[i%10] =1;
                i = i/10;
                count++;
            }while(i!=0);
            if(count==1) {
                flag[0] = 1;
            }
        }
        int count =0;
        for(int i : flag) {
            if(i ==1) count++;
            if(count>2) return false;
        }
        return true;
    }

    private static class TimeIterable implements Iterable {

        String S,T;
        int hours, minutes,seconds;
        public TimeIterable(String S, String T) {
            this.S = S;
            this.T = T;
            String[] strings = S.split(":");
            hours = Integer.parseInt(strings[0]);
            minutes = Integer.parseInt(strings[1]);
            seconds = Integer.parseInt(strings[2]);

        }
        @Override
        public Iterator iterator() {
            return new TimeIterator();
        }
        private class TimeIterator implements Iterator {

            @Override
            public boolean hasNext() {
                return !S.equals(T);
            }

            @Override
            public Boolean next() {
                //to do second 0 -10
                if(seconds<59){
                    seconds++;
                } else {
                    seconds = 0;
                    if(minutes<59) {
                        minutes++;

                    } else {
                        minutes = 0;
                        if(hours<23) {
                            hours++;
                        } else {
                            hours = 0;
                        }
                    }
                }
                boolean interesting = isInteresting(S);
                S = getTime();
                return interesting;
            }
            private String getTime() {
                StringBuilder str = new StringBuilder();
                if(hours<10) {
                    str.append(0);
                }
                str.append(hours);
                str.append(":");

                if(minutes<10) {
                    str.append(0);
                }
                str.append(minutes);

                str.append(":");

                if(seconds<10) {
                    str.append(0);
                }
                str.append(seconds);
                return str.toString();
            }

        }

    }
}

