/**
 * Created by Anmol on 2/12/2016.
 */
public class BooleanMatrixGeeks {

    public static void main(String[] args) {

        int[][] matrix = new int[][]{{1,0,0,1},
                            {0,1,0,0},
                            {0,0,0,0}};

        int[] row = new int[matrix.length];
        int[] col = new int[matrix[0].length];
        for(int i = 0; i<matrix.length;i++) {
            for(int j = 0; j< matrix[0].length;j++) {
               if(matrix[i][j] ==1) {
                   row[i] = 1;
                   col[j] = 1;
               }
            }
        }

        for(int i = 0;i<matrix.length;i++) {
            for(int j = 0; j< matrix[0].length;j++) {
                if(row[i] == 1|| col[j] == 1) {
                    matrix[i][j] = 1;
                }
            }
        }
        for (int[] aMatrix : matrix) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(aMatrix[j] + " ");
            }
            System.out.println();
        }


    }
}
