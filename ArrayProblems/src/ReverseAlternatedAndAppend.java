/**
 * Created by Anmol on 2/17/2016.
 */
public class ReverseAlternatedAndAppend {
    static class LinkedList {
        int val;
        LinkedList next;

        LinkedList(int val) {
            this.val = val;
            this.next = null;
        }
    }

    public static void main(String[] args) {
        LinkedList head = new LinkedList(1);
        head.next = new LinkedList(2);
        head.next.next = new LinkedList(3);
        head.next.next.next = new LinkedList(4);
        head.next.next.next.next = new LinkedList(5);
        head.next.next.next.next.next = new LinkedList(6);
        head.next.next.next.next.next.next = new LinkedList(7);

        reverseAlternateAndAppend(head);
    }

    private static void reverseAlternateAndAppend(LinkedList head) {
        LinkedList temp = head;
        LinkedList evenHead = null;
        LinkedList prev = null;
        int i =1;
        while (temp!=null) {
            if(i%2 ==0) {
                prev.next = temp.next;
                if(evenHead == null) {
                    evenHead = new LinkedList(temp.val);
                } else {
                    LinkedList node = new LinkedList(temp.val);
                    node.next = evenHead;
                    evenHead = node;
                }
                temp = prev.next;
            } else {
                prev = temp;
                temp = temp.next;
            }
            i++;
        }
        prev.next = evenHead;
        LinkedList print = head;
        while (print!=null) {
            System.out.print(print.val +" ");
            print = print.next;
        }
    }
}
