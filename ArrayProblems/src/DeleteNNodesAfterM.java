/**
 * Created by Anmol on 2/17/2016.
 */
public class DeleteNNodesAfterM {
    static class LinkedList {
        int val;
        LinkedList next;

        LinkedList(int val) {
            this.val = val;
            this.next = null;
        }
    }
    public static void main(String[] args) {
        LinkedList head = new LinkedList(1);
        head.next = new LinkedList(2);
        head.next.next = new LinkedList(3);
        head.next.next.next = new LinkedList(4);
        head.next.next.next.next = new LinkedList(5);
        head.next.next.next.next.next = new LinkedList(6);
        head.next.next.next.next.next.next = new LinkedList(7);

        deleteNAfterM(head, 2, 3);
    }

    private static void deleteNAfterM(LinkedList head, int m, int n) {
        LinkedList temp = head;

        int i = 0;
        while(i < m-1) {
            temp = temp.next;
            i++;
        }
        LinkedList delete = temp;
        i= 0;
        while (i<n) {
            delete = delete.next;
            i++;
        }
        temp.next = delete;
    }
}
