/**
 * Created by Anmol on 2/4/2016.
 */
public class MissingPositive {

    public static void main(String[] args) {
        int[] arr = new int[]{1,2,3,4,5};
        int size = arr.length;
        for (int i = 0; i < size; i++) {
            if (Math.abs(arr[i]) - 1 < size && arr[Math.abs(arr[i]) - 1] > 0)
                arr[Math.abs(arr[i]) - 1] = -arr[Math.abs(arr[i]) - 1];
        }

        // Return the first index value at which is positive
        for (int i = 0; i < size; i++)
            if (arr[i] > 0) {
                System.out.println(i + 1);  // 1 is added becuase indexes start from 0
                return;
            }
        System.out.println(size+1);
    }
}
