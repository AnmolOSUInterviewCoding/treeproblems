/**
 * Created by Anmol on 2/12/2016.
 */
public class SearchInRotatedSortedArray {

    public static void main(String[] args) {
        int[] arr = new int[]{5, 6, 7, 8, 9, 10, 1, 2, 3, 4};

        int element = 2;
        int end = arr.length-1;
        int start = 0;

        while(start <=end) {
            int mid = (start+end)/2;
            if(arr[mid] == element) {
                System.out.print("Element Found: " + mid);
                break;
            }

            if(arr[mid] > element) {
                if(arr[mid] > arr[end] && arr[end] > element) {
                    start = mid +1;
                } else {
                    end = mid - 1;
                }
            } else {
                start = mid +1;
            }
        }

    }
}
