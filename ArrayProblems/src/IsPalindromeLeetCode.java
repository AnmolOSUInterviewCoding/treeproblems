/**
 * Created by Anmol on 2/25/2016.
 */
public class IsPalindromeLeetCode {

    public static void main(String[] args) {
        IsPalindromeLeetCode isPalindromeLeetCode = new IsPalindromeLeetCode();
        isPalindromeLeetCode.isPalindrome(1000021);
    }
    public boolean isPalindrome(int x) {
        if(x<0) {
            return false;
        }
        int zeros = 1;

        while(x/zeros >=10) {
            zeros = zeros*10;
        }
        int rem = 0;
        int que = 0;
        while(x>0) {
            que = x/zeros;
            rem = x%10;
            if(que!= rem) return false;
            x = (x%zeros)/10;
            zeros = zeros/100;
        }
        return true;
        /*if(x<0) return false;
        int digit = 0;
        int num = x;
        while(x>=10) {
            x = x /10;
            digit++;
        }

        while(num>=10) {
            int power = (int)Math.pow(10, digit);
            int que = num/power;
            int rem = num %10;
            num = num%power;
            num = num/10;
            if(que!=rem) return false;
            digit -=2;
        }
        return true;*/

    }
}
