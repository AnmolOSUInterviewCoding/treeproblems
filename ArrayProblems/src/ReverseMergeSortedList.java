/**
 * Created by Anmol on 2/17/2016.
 */
public class ReverseMergeSortedList {
    static class LinkedList {
        int val;
        LinkedList next;

        LinkedList(int val) {
            this.val = val;
            this.next = null;
        }
    }

    public static void main(String[] args) {
        LinkedList head = new LinkedList(2);
        head.next = new LinkedList(4);
        head.next.next = new LinkedList(6);
        head.next.next.next = new LinkedList(8);
        head.next.next.next.next = new LinkedList(11);
        head.next.next.next.next.next = new LinkedList(12);
        head.next.next.next.next.next.next = new LinkedList(14);

        LinkedList head1 = new LinkedList(1);
        head1.next = new LinkedList(3);
        head1.next.next = new LinkedList(4);
        head1.next.next.next = new LinkedList(5);
        head1.next.next.next.next = new LinkedList(7);


        LinkedList node = mergeLists(head, head1);
        reverseMergedList(node);
    }

    private static void reverseMergedList(LinkedList node) {
        LinkedList curr = node, temp = node, prev = null;
        while (curr!=null) {
            curr = curr.next;
            temp.next = prev;
            prev = temp;
            temp = curr;

        }
        while (prev!=null) {
            System.out.print(prev.val +" ");
            prev = prev.next;
        }
    }

    private static LinkedList mergeLists(LinkedList head, LinkedList head1) {
        if(head == null) return head1;

        if(head1 ==null) return head;

        LinkedList node;
        if(head.val < head1.val) {
            node = new LinkedList(head.val);
            node.next = mergeLists(head.next, head1);
        } else {
            node = new LinkedList(head1.val);
            node.next = mergeLists(head, head1.next);
        }
        return node;

    }

    /*private static void reverseMergedList(LinkedList node) {
        LinkedList prev = null, temp = node, current = node;

        while(temp!=null) {
            current = current.next;
            temp.next = prev;
            prev = temp;
            temp = current;
        }

    }

    private static LinkedList mergeLists(LinkedList head, LinkedList head1) {
        if(head == null) return head1;
        if(head1 == null) return head;

        LinkedList node;
        if(head.val < head1.val) {
            node = new LinkedList(head.val);
            node.next = mergeLists(head.next, head1);
        } else {
            node = new LinkedList(head1.val);
            node.next = mergeLists(head, head1.next);
        }
        return node;
    }*/
}
