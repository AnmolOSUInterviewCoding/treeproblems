/**
 * Created by Anmol on 2/17/2016.
 */
public class DeleteNodesWithGreaterRightValue {

    static class LinkedList {
        int val;
        LinkedList next;

        LinkedList(int val) {
            this.val = val;
            this.next = null;
        }
    }
    public static void main(String[] args) {
        LinkedList head = new LinkedList(12);
        head.next = new LinkedList(15);
        head.next.next = new LinkedList(10);
        head.next.next.next = new LinkedList(9);
        head.next.next.next.next = new LinkedList(11);
        head.next.next.next.next.next = new LinkedList(5);
        head.next.next.next.next.next.next = new LinkedList(6);
        head.next.next.next.next.next.next.next = new LinkedList(2);
        head.next.next.next.next.next.next.next.next = new LinkedList(3);

        deleteGreaterRightValues(head);
    }

    private static void deleteGreaterRightValues(LinkedList head) {
        LinkedList node = reverse(head);

        LinkedList print = reverse(deleteNodes(node));
        while (print!=null ){
            System.out.print(print.val +" ");
            print = print.next;
        }
    }

    private static LinkedList deleteNodes(LinkedList node) {
        LinkedList currMax = node;
        LinkedList temp = node.next;

        while(temp!=null) {
            if(currMax.val > temp.val ) {
                currMax.next = temp.next;
                temp = currMax.next;
            } else {
                currMax = temp;
                temp = temp.next;
            }
        }
        return node;
    }

    private static LinkedList reverse(LinkedList head) {
        LinkedList prev= null, temp = head, curr = head;
        while (temp!=null) {
            curr = curr.next;
            temp.next = prev;
            prev = temp;
            temp = curr;
        }
        return prev;
    }
}
