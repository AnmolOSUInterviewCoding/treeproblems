/**
 * Created by Anmol on 2/12/2016.
 */
public class OddOccurenceGeeks {

    public static void main(String[] args) {
        int arr[] = new int[]{1,2,3,4,3,2,1,3,4};

        int temp = 0;
        for(int i = 0 ;i<arr.length;i++) {
            temp = temp^ arr[i];
        }
        System.out.print("Odd Element: " + temp);
    }
}
