/**
 * Created by Anmol on 2/16/2016.
 */
public class BasicCalculator {
    public static void main(String[] args) {
        String s = "3+2*2";
        int left = -1;
        int right = -1;
        char operator= ' ';
        int result = 0;
        for(int i = s.length()-1;i>=0;i--) {
            char ch = s.charAt(i);
            if(Character.isDigit(ch)) {
                if(left==-1) {
                    left = Character.getNumericValue(ch);
                } else {
                    right = Character.getNumericValue(ch);
                    switch(operator) {
                        case '+':
                            result = left+ right;
                            break;
                        case '-':
                            result = right - left;
                            break;
                        case '*':
                            result = left* right;
                            break;
                        case '/':
                            result = right / left;

                            break;
                    }
                    left = result;
                }
            } else {
                operator = ch;
            }
        }
    }
}
