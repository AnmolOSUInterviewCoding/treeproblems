/**
 * Created by Anmol on 2/3/2016.
 */
public class TurnImageBy90Degrees {
    public static void main(String[] args) {
        int[][] b = new int[5][6];
        int[][] result = new int[6][5];

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 6; j++) {
                b[i][j] = i * j;
            }
        }


        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 6; j++) {
                System.out.print(b[i][j]+ "\t ");
            }
            System.out.println();

        }

        for(int i = 0; i< 5; i++) {
            for(int j = 0; j <6;j++) {
                result[j][5-i-1] = b[i][j];
            }
        }
        System.out.println();
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 5; j++) {
                System.out.print(result[i][j]+ "\t ");
            }
            System.out.println();

        }
    }
}

