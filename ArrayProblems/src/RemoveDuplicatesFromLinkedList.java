import com.sun.scenario.effect.impl.state.LinearConvolveKernel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Anmol on 2/13/2016.
 */
public class RemoveDuplicatesFromLinkedList {
    static class LinkedList {
        int val;
        LinkedList next;

        LinkedList(int val) {
            this.val = val;
            this.next = null;
        }
    }

    public static void main(String[] args) {
        LinkedList head = getLinkedList();

        LinkedList newHead = removeElements(head, 1);
//        LinkedList nonDuplicates = removeAllDuplicates(head);
        LinkedList duplicates = removeAllDuplicatesNoSpace(head);
    }

    public static LinkedList getLinkedList() {
        LinkedList head = new LinkedList(1);
        head.next = new LinkedList(1);
        head.next.next = new LinkedList(1);
        head.next.next.next = new LinkedList(2);
        head.next.next.next.next = new LinkedList(5);
        head.next.next.next.next.next = new LinkedList(4);
        head.next.next.next.next.next.next = new LinkedList(1);
        head.next.next.next.next.next.next.next = new LinkedList(1);
        head.next.next.next.next.next.next.next.next = new LinkedList(1);
        head.next.next.next.next.next.next.next.next.next = new LinkedList(5);
        return head;
    }

    private static LinkedList removeAllDuplicatesNoSpace(LinkedList head) {
        LinkedList newHead = head;
        LinkedList temp;
        while(head!= null) {
            temp = head.next;
            LinkedList current = head;
            while(temp!=null) {
                if(temp.val != head.val) {
                    current.next = temp;
                    current = temp;
                }
                temp= temp.next;
            }
            current.next = temp;
            head = head.next;
        }
        return newHead;
    }

    private static LinkedList removeAllDuplicates(LinkedList head) {
        List<Integer> list = new ArrayList<Integer>();
        LinkedList temp = null, newHead = head;

        while(head!=null) {
            if(!list.contains(head.val)) {
                temp = head;
                list.add(head.val);
            } else {
                temp.next = head.next;
            }
            head = head.next;
        }
        return newHead;

    }

    private static LinkedList removeElements(LinkedList head, int val) {
        LinkedList temp = head;
        while (temp.val == val){
            temp = temp.next;
            head = temp;
        }
        LinkedList current = null;
        while(temp!=null) {
            while (temp!=null && temp.val!=val) {
                current = temp;
                temp = temp.next;
            }
            if(temp == null) return head;
            current.next = temp.next;
            temp = current.next;

        }
        return head;
    }
}
