import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Anmol on 2/20/2016.
 */
public class THreeSumLeetCode {

    public static void main(String[] args) {
        int[] nums = new int[]{-1, 0, 0, 0, 0, 0, 0, 0, 1};
        List<List<Integer>> lists = threeSum(nums);
    }

    public static List<List<Integer>> threeSum(int[] nums) {
        Arrays.sort(nums);
        List<List<Integer>> result = new ArrayList<List<Integer>>();

        int sum = 3;
        boolean allZero = true;
        if (nums.length < 3) {
            return result;
        }
        for (int k = 0; k < nums.length; k++) {
            if (nums[k] != 0) {
                allZero = false;
                break;
            }
        }
        if (allZero) {
            List<Integer> numbers = new ArrayList<Integer>();
            numbers.add(0);
            numbers.add(0);
            numbers.add(0);
            result.add(numbers);
            return result;
        }
        for (int i = 0; i <= nums.length - 3; i++) {
            int j = i + 1;
            int end = nums.length - 1;
            while (j < end) {
                sum = nums[i] + nums[j] + nums[end];
                if (sum < 0) {
                    j++;
                } else if (sum > 0) {
                    end--;
                } else {
                    List<Integer> numbers = new ArrayList<Integer>();
                    numbers.add(nums[i]);
                    numbers.add(nums[j]);
                    numbers.add(nums[end]);
                    if (!result.contains(numbers))
                        result.add(numbers);

                    j++;
                    end--;
                }
            }
        }
        return result;
    }
}
