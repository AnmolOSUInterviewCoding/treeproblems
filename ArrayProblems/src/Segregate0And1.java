/**
 * Created by Anmol on 2/2/2016.
 */
public class Segregate0And1 {

    public static void main(String[] args) {
        int arr[] = new int[]{0,1,0,1,0,0,1,0,1,0,1,0};

        int i =0,j=0;
        while (j<arr.length) {
            if(arr[j] ==0) {
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
                i++;
                j++;
            } else {
                j++;
            }
        }
        for(int k : arr){
            System.out.print(k +" ");
        }
    }
}
