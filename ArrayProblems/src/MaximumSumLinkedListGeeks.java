/**
 * Created by Anmol on 2/17/2016.
 */
// Java program to construct a Maximum Sum Linked List out of
// two Sorted Linked Lists having some Common nodes
class MaximumSumLinkedListGeeks
{
    /* Linked list Node*/
    static class Node
    {
        int val;
        Node next;
        Node(int d)
        {
            val = d;
            next = null;
        }
    }

    // Method to adjust pointers and print final list
    static void finalMaxSumList(Node a, Node b)
    {
        Node result = null;

        /* assigning pre and cur to head
           of the linked list */
        Node pre1 = a, curr1 = a;
        Node pre2 = b, curr2 = b;

        /* Till either of current pointers is not null
           execute the loop */
        while (curr1 != null || curr2 != null)
        {
            // Keeping 2 local variables at the start of every
            // loop run to keep track of the sum between pre
            // and cur reference elements.
            int sum1 = 0, sum2 = 0;

            // Calculating sum by traversing the nodes of linked
            // list as the merging of two linked list.  The loop
            // stops at a common node
            while (curr1 != null && curr2 != null &&
                    curr1.val != curr2.val)
            {

                if (curr1.val <curr2.val)
                {
                    sum1 += curr1.val;
                    curr1 = curr1.next;
                }
                else
                {
                    sum2 += curr2.val;
                    curr2 = curr2.next;
                }
            }

            // If either of current pointers becomes null
            // carry on the sum calculation for other one.
            if (curr1 == null)
            {
                while (curr2 != null)
                {
                    sum2 += curr2.val;
                    curr2 = curr2.next;
                }
            }
            if (curr2 == null)
            {
                while(curr1 != null)
                {
                    sum1 += curr1.val;
                    curr1 = curr1.next;
                }
            }

            // First time adjustment of resultant head based on
            // the maximum sum.
            if (pre1 == a && pre2 == b)
                result = (sum1 > sum2) ? pre1 : pre2;

                // If pre1 and pre2 don't contain the head refernces of
                // lists adjust the next pointers of previous pointers.
            else
            {
                if (sum1 > sum2)
                    pre2.next = pre1.next;
                else
                    pre1.next = pre2.next;
            }

            // Adjusting previous pointers
            pre1 = curr1;
            pre2 = curr2;

            // If curr1 is not NULL move to the next.
            if (curr1 != null)
                curr1 = curr1.next;

            // If curr2 is not NULL move to the next.
            if (curr2 != null)
                curr2 = curr2.next;
        }

        while (result != null)
        {
            System.out.print(result.val + " ");
            result = result.next;
        }
        System.out.println();
    }



    /* Drier program to test above functions */
    public static void main(String args[])
    {
        Node head1 = new Node(1);
        head1.next = new Node(3);
        head1.next.next = new Node(20);
        head1.next.next.next = new Node(90);
        head1.next.next.next.next = new Node(110);
        head1.next.next.next.next.next = new Node(120);
        //Linked List 1 : 1->3->30->90->110->120->NULL
        //Linked List 2 : 0->3->12->32->90->100->120->130->NULL
        Node head2 = new Node(0);
        head2.next = new Node(3);
        head2.next.next = new Node(12);
        head2.next.next.next = new Node(32);
        head2.next.next.next.next = new Node(90);
        head2.next.next.next.next.next = new Node(100);
        head2.next.next.next.next.next.next = new Node(120);
        head2.next.next.next.next.next.next.next = new Node(130);
        finalMaxSumList(head1, head2);
    }
}
