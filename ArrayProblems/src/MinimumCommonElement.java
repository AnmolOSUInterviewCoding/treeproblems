import java.util.HashMap;

/**
 * Created by Anmol on 2/14/2016.
 */
public class MinimumCommonElement {

    public static void main(String[] args) {
        int[] arr1 = new int[]{5,2,7,1,45,3};

        int[] arr2 = new int[]{2,1,3,55,32,67,10};

        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();

        for(int i = 0;i<arr1.length;i++) {
            map.put(arr1[i], 1);
        }
        int min = Integer.MAX_VALUE;
        for(int i = 0; i<arr2.length;i++) {
            if(map.containsKey(arr2[i]) && arr2[i] < min) {
                min = arr2[i];
            }
        }

        System.out.print("Min Common is: " + min);

    }
}
