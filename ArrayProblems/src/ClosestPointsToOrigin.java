import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

/**
 * @author  by Anmol on 10/22/2016.
 */
public class ClosestPointsToOrigin {


    public static void main(String[] args) {
        List<Point> pointList = new ArrayList<>();
        Point origin = new Point(0, 0);
        pointList.add(new Point(1,1, origin));
        pointList.add(new Point(1,2, origin));
        pointList.add(new Point(1,3, origin));
        pointList.add(new Point(-1,1, origin));
        pointList.add(new Point(1,-1, origin));
        pointList.add(new Point(1,-2, origin));
        pointList.add(new Point(1,4, origin));
        pointList.add(new Point(1,5, origin));

        ClosestPointsToOrigin closestPointsToOrigin = new ClosestPointsToOrigin();
        PriorityQueue<Point> points = closestPointsToOrigin.getClosesPoint(pointList, 3);
        System.out.print(points);
    }

    private PriorityQueue<Point> getClosesPoint(List<Point> pointList, int i) {
        PriorityQueue<Point> queue = new PriorityQueue<>();
        for(Point point : pointList) {
            if(queue.size() < i) {
                queue.offer(point);
            } else if(queue.peek().compareTo(point) < 0) {
                queue.poll();
                queue.offer(point);
            }
        }
        return queue;
    }
}

class Point implements Comparable<Point> {

    int x, y;
    double dist;

    Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    Point(int x, int y, Point point) {
        this.x = x;
        this.y = y;
        this.dist = Math.sqrt((point.x - x) * (point.x - x) + (point.y - y) * (point.y - y));
    }


    @Override
    public int compareTo(Point o) {
        return Double.valueOf(o.dist).compareTo(this.dist);
    }

    @Override
    public String toString() {
        return "[X: " + x + " Y: " + y +" ]";
    }

}