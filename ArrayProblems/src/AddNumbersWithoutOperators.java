/**
 * Created by Anmol on 2/14/2016.
 */
public class AddNumbersWithoutOperators {

    public static void main(String[] args) {
        int a = 3,b = 2;

        while (b != 0){
            int carry = (a & b) ; //CARRY is AND of two bits
            System.out.print(a+ " ");

            a = a ^b; //SUM of two bits is A XOR B
            System.out.print(a+ " ");

            b = carry << 1; //shifts carry to 1 bit to calculate sum
            System.out.print(b+ " ");

        }

    }
}
