/**
 * Created by Anmol on 2/3/2016.
 */
public class ReverseWordsInString {

    public static void main(String[] args) {
        String s = "the sky is   blue";
        s = s.trim();
//        if(s.length()==1) return s;
        String[] split = s.split(" ");
        int j = split.length -1;
        StringBuilder my = new StringBuilder();
        for(int i = 0; i<=j;i++) {
            if(split[i].equals("")){
                continue;
            } else {
                String temp = split[i];
                split[i] = split[j];
                split[j] = temp;
                j--;
            }

        }
        for(int i =0; i< split.length -1;i++) {
            if(!split[i].equals("")) {
                my.append(split[i]);
                my.append(" ");
            }
        }
        my.append(split[split.length -1]);
        System.out.println(my );
    }
}
