import java.io.*;

/**
 * Created by Anmol on 4/19/2016.
 */
public class SlowStart {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("C:\\Users\\Anmol\\ArrayProblems\\src\\large_transfer"));
        } catch (FileNotFoundException e) {
            System.exit(-1);
        }
        String line;
        int packetSize = 536;
        int congestionDifference;
        PrintWriter fw = new PrintWriter(new FileWriter("C:\\Users\\Anmol\\ArrayProblems\\src\\slow.txt"));
        PrintWriter printWriter = new PrintWriter(new FileWriter("C:\\Users\\Anmol\\ArrayProblems\\src\\out.txt"));
        while ((line = reader.readLine()) != null) {
            String[] split = line.split("\t");
            int oldVal = Integer.parseInt(split[1]);
            int newVal = Integer.parseInt(split[2]);
            if(newVal > oldVal && (newVal == packetSize || (newVal-oldVal == packetSize))) {
                fw.write(line + "\n");
            } else {
                printWriter.write(line + "\n");
            }
        }
        fw.close();
        printWriter.close();
    }
}
