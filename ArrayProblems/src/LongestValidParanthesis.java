import java.util.Stack;

/**
 * Created by Anmol on 2/12/2016.
 */
public class LongestValidParanthesis {
    public static void main(String[] args) {
        String paran = "()(())";
        /*int match = 0;
        Stack<Character> stack = new Stack<Character>();

        for(int i = 0 ;i<paran.length();i++) {
            char ch = paran.charAt(i);
            if(ch == ')') {
                if(!stack.empty()) {
                    if(stack.peek() == '(') match++;
                    stack.pop();
                }
                if(i == paran.length()-1 && stack.peek() == '(') match--;
            } else {
                stack.push(ch);
            }
        }*/
        Stack<int[]> stack = new Stack<int[]>();
        int result = 0;

        for(int i=0; i<=paran.length()-1; i++){
            char c = paran.charAt(i);
            if(c=='('){
                int[] a = {i,0};
                stack.push(a);
            }else{
                if(stack.empty()||stack.peek()[1]==1){
                    int[] a = {i,1};
                    stack.push(a);
                }else{
                    stack.pop();
                    int currentLen=0;
                    if(stack.empty()){
                        currentLen = i+1;
                    }else{
                        currentLen = i-stack.peek()[0];
                    }
                    result = Math.max(result, currentLen);
                }
            }
        }
        System.out.print(result);
    }
}
