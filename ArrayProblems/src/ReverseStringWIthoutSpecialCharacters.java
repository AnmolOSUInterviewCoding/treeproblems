/**
 * Created by Anmol on 2/16/2016.
 */
public class ReverseStringWIthoutSpecialCharacters {

    public static void main(String[] args) {
        String str = "Abg,c,de!$";
        char[] chars = str.toCharArray();
        int start = 0;
        int end = chars.length-1;
        while (start<end) {
            if(Character.isAlphabetic(chars[start])&&Character.isAlphabetic(chars[end])) {
                char temp = chars[start];
                chars[start] = chars[end];
                chars[end] = temp;
                start++;
                end--;
            } else if(Character.isAlphabetic(chars[start])){
                end--;
            } else {
                start++;
            }
        }
        for(char ch : chars) {
            System.out.print(ch+" ");
        }
    }
}
