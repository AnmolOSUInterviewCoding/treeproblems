/**
 * Created by Anmol on 5/8/2016.
 */
public class JumpGameLeetCode {
    public static void main(String[] args) {
        int[] nums = new int[]{2,1,1,1,4};
        int max = nums[0];
        for(int i = 0;i<nums.length;i++) {
            //i>= max means this the till where u could go max and if it is 0 then no more jumps.
            if(i>=max && nums[i] ==0) {
                System.out.print("false");
                return;
            }
            if(i + nums[i] > max){
                max = i+ nums[i];
            }
            if(max >= nums.length -1) {
                System.out.print("true");
                return;
            }
        }
        System.out.print("false");

    }
}
