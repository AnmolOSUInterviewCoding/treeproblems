/**
 * Created by Anmol on 2/20/2016.
 */
public class MaximumSumSubArray {
    public static void main(String[] args) {
        int[] nums = new int[]{-2,1,-3,4,-1,2,1,-5,4};

        int newsum=nums[0];
        int max=nums[0];
        for(int i=1;i<nums.length;i++){
            newsum=Math.max(newsum+nums[i],nums[i]);
            max= Math.max(max, newsum);
        }
    }
}
