/**
 * Created by Anmol on 2/13/2016.
 */
public class PartitionLinkList {

    static class LinkedList {
        int val;
        LinkedList next;

        LinkedList(int val) {
            this.val = val;
            this.next = null;
        }
    }

    public static void main(String[] args) {
        LinkedList head = new LinkedList(3);
        head.next = new LinkedList(1);
        head.next.next = new LinkedList(1);
        head.next.next.next = new LinkedList(2);
        head.next.next.next.next = new LinkedList(5);
        head.next.next.next.next.next = new LinkedList(4);
        head.next.next.next.next.next.next = new LinkedList(6);
        moveList(head);
    }

    private static void moveList(LinkedList head) {
        int value = 3;
        LinkedList l1 = null;
        LinkedList l2 = null;
        LinkedList l1Head = null;
        LinkedList l2Head = null;
        while (head!=null) {
            if(head.val < value) {
                if(l1Head == null) {
                    l1Head = new LinkedList(head.val);
                    l1 = l1Head;
                } else {
                    l1.next = new LinkedList(head.val);
                    l1 = l1.next;
                }
            } else {
                if(l2Head == null) {
                    l2Head = new LinkedList(head.val);
                    l2 = l2Head;
                } else {
                    l2.next = new LinkedList(head.val);
                    l2 = l2.next;
                }
            }
            head = head.next;
        }

        /*add case for if all the elements of list are greater than value.
        in that case l1 will be null so just return l2Head.*/
        l1.next = l2Head;

    }
}
