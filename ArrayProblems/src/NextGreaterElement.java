import java.util.Stack;

/**
 * Created by Anmol on 2/3/2016.
 */
public class NextGreaterElement {
    public static void main(String[] args) {
        int[] arr = new int[]{4, 5, 2, 25};

        Stack<Integer> stack = new Stack<Integer>();

        stack.push(arr[0]);

        for(int i =1; i< arr.length;i++) {
            if(stack.peek() < arr[i]) {
                while(!stack.empty()&&stack.peek() < arr[i]) {
                    int temp = stack.pop();
                    System.out.println(temp + "--->" + arr[i]);
                }
                stack.push(arr[i]);
            } else {
                stack.push(arr[i]);
            }
        }
        while(!stack.empty()) {
            System.out.println(stack.pop() + "--->" + -1 );
        }
    }
}
