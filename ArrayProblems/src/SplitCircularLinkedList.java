/**
 * Created by Anmol on 2/17/2016.
 */
public class SplitCircularLinkedList {
    static class LinkedList {
        int val;
        LinkedList next;

        LinkedList(int val) {
            this.val = val;
            this.next = null;
        }
    }
    public static void main(String[] args) {
        LinkedList head = new LinkedList(12);
        head.next = new LinkedList(15);
        head.next.next = new LinkedList(10);
        head.next.next.next = new LinkedList(9);
        head.next.next.next.next = new LinkedList(11);
        head.next.next.next.next.next = new LinkedList(5);
        head.next.next.next.next.next.next = new LinkedList(6);
        head.next.next.next.next.next.next.next = new LinkedList(2);
        head.next.next.next.next.next.next.next.next = head;
        splitCircularLinkedList(head);
    }

    private static void splitCircularLinkedList(LinkedList head) {
        LinkedList slow = head;
        LinkedList fast = head;
        while (fast.next!=head && fast.next.next!=head) {
            fast = fast.next.next;
            slow = slow.next;
        }
        LinkedList newHead = slow.next;
        if(fast.next == head) fast.next = newHead;
        else fast.next.next = newHead;

        slow.next = head;

    }
}
