/**
 * Created by Anmol on 10/17/2016.
 */
public class SlidingWindowMaximum {

    public static void main(String[] args) {
        int[] nums = new int[]{1,3,-1,-3,5,3,6,7};
        SlidingWindowMaximum slidingWindowMaximum = new SlidingWindowMaximum();
        int[] arr = slidingWindowMaximum.maxSlidingWindow(nums, 3);
        oddeven(19);/*
        for(int i : arr) {
            System.out.print(i +" ");
        }*/
    }

    private static void oddeven(int i) {
        while (true) {
            if(i == 1) {
                break;
            } else if (i%2 == 0) {
                i = i/2;
            } else {
                i = 3*i +1;
            }
            System.out.print(i +" ");
        }
        System.out.println(i);
    }


    public int[] maxSlidingWindow(int[] nums, int k) {
        if(nums.length == 0 ) return new int[0];
        int max = 0;
        int[] result = new int[nums.length - k +1];
        if(nums.length ==1 && k==1)return new int[]{nums[0]};
        for(int i = 0; i<nums.length; i++) {
            if(i<k-1) continue;
            if(i == k-1) {
                max = getMax(nums, k, i-k+1);
                result[i-k+1] = max;
            } else {
                if(nums[i] == max) {
                    max = Math.max(max, nums[i]);
                } else {
                    max = getMax(nums, k, i-k+1);
                }
                result[i-k+1] = max;
            }

        }
        return result;
    }

    public int getMax(int[] nums,int k, int start) {
        int m = Integer.MIN_VALUE;
        int i;
        for(i = start; i< start + k; i++) {
            if(m< nums[i]) {
                m = nums[i];
            }
        }
        return Math.max(m, nums[i-1]);
    }
}
