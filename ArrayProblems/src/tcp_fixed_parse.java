import java.io.*;

/**
 * Created by Anmol on 4/20/2016.
 */
public class tcp_fixed_parse {

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader("C:\\Users\\Anmol\\ArrayProblems\\src\\tcp_fixed_output"));
        } catch (FileNotFoundException e) {
            System.out.println("File Not found!!");
            System.exit(-1);
        }

        PrintWriter fw = new PrintWriter(new FileWriter("C:\\Users\\Anmol\\ArrayProblems\\src\\fixedCWND.txt"));

        String line;
        CharSequence sequence1 = "Value of CWND is";
        while ((line = bufferedReader.readLine())!=null) {
            if (line.contains(sequence1)) {
                String[] par = line.split(" ");
                fw.write(par[0] + "\t" + par[5] +"\n");
            }
        }
        fw.close();
        bufferedReader.close();
    }

}