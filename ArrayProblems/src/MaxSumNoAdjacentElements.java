/**
 * Created by Anmol on 2/1/2016.
 */
public class MaxSumNoAdjacentElements {

    public static void main(String[] args) {
        int[] arr = new int[]{5,  5, 10, 40, 50, 35};
//        int[] arr = new int[]{3, 2 ,7 ,10};
        int including = arr[0];
        int excluding = 0;
        int[] result = new int[arr.length];
//        result[0]= arr[0];
//        result[1] = arr[1];
        for(int i =1;i<arr.length;i++) {
            int temp = including > excluding ? including : excluding;
            including = arr[i] + excluding;
            excluding = temp;
//            result[i] = Math.max(result[i-2] + arr[i], result[i-1]);
        }
        System.out.print(Math.max(including, excluding));
//        System.out.print(result[arr.length-1]);
    }
}
