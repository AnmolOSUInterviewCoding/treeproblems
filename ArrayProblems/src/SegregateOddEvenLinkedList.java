/**
 * Created by Anmol on 2/17/2016.
 */
public class SegregateOddEvenLinkedList {

    static class LinkedList {
        int val;
        LinkedList next;

        LinkedList(int val) {
            this.val = val;
            this.next = null;
        }
    }
    public static void main(String[] args) {
        LinkedList head = new LinkedList(1);
        head.next = new LinkedList(2);
        head.next.next = new LinkedList(3);
        head.next.next.next = new LinkedList(4);
        head.next.next.next.next = new LinkedList(5);
        head.next.next.next.next.next = new LinkedList(6);
        head.next.next.next.next.next.next = new LinkedList(7);
//        head.next.next.next.next.next.next.next = new LinkedList(8);

        segregateOddEven(head);

    }

    private static void segregateOddEven(LinkedList head) {
        LinkedList temp=head;
        while (temp.next!=null) {
            temp = temp.next;
        }
        LinkedList end = temp;
        LinkedList newHead = null;
        LinkedList curr = head, next, prev = null;
        while (curr!=end) {

            if(curr.val%2 != 0) {

                next = curr.next;
                if(prev!=null) {
                    prev.next = next;
                }
                temp.next = curr;
                curr.next = null;
                temp = curr;
                curr = next;
            } else {
                if(newHead == null) {
                    newHead = curr;

                }
                prev = curr;
                curr = curr.next;
            }
        }
        temp.next = null;

    }
}
