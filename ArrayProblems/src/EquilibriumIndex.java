/**
 * Created by Anmol on 2/3/2016.
 */
public class EquilibriumIndex {

    public static void main(String[] args) {
        int[] arr = new int[]{-7, 1, 5, 2, -4, 3, 0};
        int sum = 0;
        for (int i = 0; i < arr.length; i++) sum += arr[i];
        int leftSum = 0;
        int rightSum = 0;
        for (int i = 0; i < arr.length; i++) {
            if (i > 0) {
                leftSum += arr[i - 1];
            }
            rightSum = sum - leftSum - arr[i];
            if (leftSum == rightSum) System.out.print(i + " ");
        }
    }

}
