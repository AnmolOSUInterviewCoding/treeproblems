/**
 * Created by Anmol on 2/17/2016.
 */
public class MergeLinkedListToAnotherGeeks {
    static class LinkedList {
        int val;
        LinkedList next;

        LinkedList(int val) {
            this.val = val;
            this.next = null;
        }
    }

    public static void main(String[] args) {
        LinkedList head = new LinkedList(1);
        head.next = new LinkedList(2);
        head.next.next = new LinkedList(3);

        LinkedList head2 = new LinkedList(4);
        head2.next= new LinkedList(5);
        head2.next.next= new LinkedList(6);
        head2.next.next.next = new LinkedList(7);
        head2.next.next.next.next = new LinkedList(8);

        mergeListWithEachOther(head, head2);
    }

    private static void mergeListWithEachOther(LinkedList head, LinkedList head2) {

        LinkedList curr1 = head, curr2 = head2;
        LinkedList h1Next, h2Next;
        while(curr1!=null && curr2!=null){
            h1Next = curr1.next;
            h2Next = curr2.next;

            curr1.next = curr2;
            curr2.next = h1Next;

            curr1 = h1Next;
            curr2 = h2Next;
        }
        head2 = curr2;
    }
}
