/**
 * Created by Anmol on 2/2/2016.
 */
public class BuyAndSellStocks {

    public static void main(String[] args) {
        int[] prices = new int[]{3,4,1,5,6,3,9};
        int buyPrice = prices[0];
        int profit = 0;
        for(int i =1;i< prices.length;i++) {
            if(prices[i]> buyPrice && profit < prices[i] - buyPrice) {
                profit = prices[i] - buyPrice;
            } else if(prices[i] < buyPrice) {
                buyPrice = prices[i];
            }
        }
        System.out.print(profit);
    }
}
